grammar Expressions;

// Parser Rules

statement
    : expression
    | let
    | solve
    ;

let
    : LET (('const' constant) | variable) 'be'? 'in' set
    ;

solve
    : SOLVE expression
    ;

set
    : SET
    ;
 
expression
    : primary                                            # aPrimary
    | composite                                          # aComposite
    | expression '^' expression                          # exponentiation
    | '-' expression                                     # negation
    | expression '/' expression                          # division
    | expression '*' expression                          # multiplication
    | simpleMultiplication                               # aSimpleMultiplication
    | expression operator=('+' | '-') expression         # additive
    | expression '=' expression                          # equation
    ;

primary
    : number
    | variable
    ;

composite
    : '(' expression ')'                                 # parentheses
    | '|' expression '|'                                 # abs
    | function                                           # aFunction
    ;

simpleMultiplication
    : primary primary
    | primary composite
    | primary expression
    | composite primary
    | composite composite
    ;

function
    : FUNC '(' expression ')'
    ;

variable
    : SingleCharIdentifier
    ;

constant
    : SingleCharIdentifier
    ;

number
    : NATURAL
    ;
 
// Lexer Rules
 
FUNC : 'sqrt' | 'exp' | 'ln' | 'log' | 'abs' ;
SET  : 'N' | 'Z' | 'Q' | 'R' ;

ADD  : '+' ;
SUB  : '-' ;

LET  : 'let' ;
SOLVE: 'solve';

NATURAL : [0-9]+ ;

SingleCharIdentifier
    : [a-zA-Z]
    ;
 
WS  : [ \t\r\n]+ -> skip ;