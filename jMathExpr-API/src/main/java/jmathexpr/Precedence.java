/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr;

/**
 * Enumeration type for expression precedence values (returned by
 * Expression#getPrecedence()). Lower integral value means higher precedence
 * (that must be evaluated earlier). Consequently if an operation contains a lower
 * precedence argument that argument must be surrounded in parentheses (in string
 * representation).
 * 
 * The integral precedence values are based on
 * {@link http://en.wikipedia.org/wiki/C_operator_precedence#Operator_precedence}.
 *
 * @author Elemér Furka
 */
public enum Precedence {

    Evaluation(0), Function(1), Exponentiation(2), UnaryOperation(3),
    Multiplication(5), Addition(6),
    Comparison(8), Equality(9), And(13), Or(14), Assignement(16);

    private final int value;

    private Precedence(int value) {
        this.value = value;
    }
    
    /**
     * Tests which Precedence value is lower. Lower precedence expressions must
     * be evaluated later!
     * 
     * @return true if this Precedence represents a lower value, e.g. the appropriate
     * operation must be evaluated later
     */
    public boolean lt(Precedence other) {
        return value > other.value;
    }
    
    /**
     * Tests which Precedence value is lower or if they have the same value.
     * Lower precedence expressions must
     * be evaluated later!
     */
    public boolean le(Precedence other) {
        return value >= other.value;
    }
}
