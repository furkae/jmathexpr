/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.real;

import jmathexpr.Expression;
import jmathexpr.bool.TruthValue;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.integer.Integers;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.rational.Rationals;
import jmathexpr.arithmetic.real.impl.DoubleRealFactory;
import jmathexpr.arithmetic.real.impl.DoubleRealFunctions;
import jmathexpr.set.Cardinality;
import jmathexpr.set.Set;

/**
 * The set of all real numbers.
 * 
 * @author Elemér Furka
 */
public class Reals extends Numbers implements RealFactory {
    
    private static final Reals INSTANCE = new Reals();
    
    private final RealFactory factory;
    
    private final RealFunctions functions;
    
    private Reals() {
        this(new DoubleRealFactory(), new DoubleRealFunctions());
    }
    
    private Reals(RealFactory factory, RealFunctions functions) {
        super('R', "\u211D");
        
        this.factory = factory;
        this.functions = functions;
    }
    
    @Override
    public Reals evaluate() {
        return this;
    }
    
    public static Reals getInstance() {
        return INSTANCE;
    }

    @Override
    public TruthValue contains(Expression element) {
        return element instanceof ANumber ? TruthValue.valueOf(((ANumber) element).isReal()) : TruthValue.False;
    }

    @Override
    public boolean subsetOf(Set set) {
        if (equals(set)) {
            return true;
        } else if (set.equals(Rationals.getInstance()) || set.equals(Integers.getInstance())
                || set.equals(Naturals.getInstance())) {
            return false;
        }
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RealNumber create(String asString) {
        return factory.create(asString);
    }

    @Override
    public RealNumber create(double asDouble) {
        return factory.create(asDouble);
    }
    
    public RealFunctions functions() {
        return functions;
    }

    @Override
    public Set domain() {
        return this;
    }

    @Override
    public Set codomain() {
        return this;
    }

    @Override
    public Cardinality cardinality() {
        return Cardinality.aleph(1);
    }
}
