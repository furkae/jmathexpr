/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.real;

import jmathexpr.AbstractExpression;
import jmathexpr.Precedence;
import jmathexpr.util.rule.Rule;

/**
 * The symbols positive and negative infinity.
 * 
 * @author Elemér Furka
 */
public class Infinity extends AbstractExpression {
    
    public static final Infinity PLUS_INFINITY = new Infinity(false);
    public static final Infinity MINUS_INFINITY = new Infinity(true);
    
    private final boolean negative;
    
    private Infinity(boolean negative) {
        this.negative = negative;
    }
    
    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (this == object) return true;
        if (object instanceof Infinity) {
            return negative == ((Infinity) object).negative;
        }
        
        return false;
    }
    
    @Override
    public String toString() {
        return negative ? "-" + "inf" : "+" + "inf";
    }

    @Override
    public String toUnicode() {
        return negative ? "-" + "\u221E" : "+" + "\u221E";
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public boolean isApplicable(Rule rule) {
        return rule.matches(this);
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }
}
