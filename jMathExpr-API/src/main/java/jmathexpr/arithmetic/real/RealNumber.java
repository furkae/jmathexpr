/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.real;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.integer.IntegerNumber;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.rational.RationalNumber;
import jmathexpr.set.Set;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.rule.Rule;

/**
 * Abstract representation of a real number.
 * 
 * @author Elemér Furka
 */
public abstract class RealNumber extends AbstractExpression
        implements ANumber, ExpressionPattern, Comparable<ANumber> {
    
    @Override
    public RealNumber evaluate() {
        return this;
    }

    @Override
    public boolean isNatural() {
        return false;
    }

    @Override
    public boolean isInteger() {
        return false;
    }

    @Override
    public boolean isRational() {
        return false;
    }

    @Override
    public boolean isReal() {
        return true;
    }

    @Override
    public RealNumber toReal() {
        return this;
    }
    
    @Override
    public RationalNumber toRational() {
        throw new UnsupportedOperationException("Cannot convert a Real to a Rational");
    }

    @Override
    public IntegerNumber toInteger() {
        throw new UnsupportedOperationException("Cannot convert a Real to an Integer");
    }

    @Override
    public NaturalNumber toNatural() {
        throw new UnsupportedOperationException("Cannot convert a Real to a Natural");
    }

    @Override
    public boolean isConstant() {
        return true;
    }
    
    @Override
    public boolean contains(ExpressionPattern pattern) {
        return pattern.matches(this);
    }

    @Override
    public boolean isApplicable(Rule rule) {
        return rule.matches(this);
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }

    @Override
    public Set domain() {
        return Reals.getInstance();
    }

    @Override
    public Set codomain() {
        return Reals.getInstance();
    }

    @Override
    public boolean matches(Expression expr) {
        return equals(expr);
    }

    @Override
    public int compareTo(ANumber o) {
        if (equals(o)) {
            return 0;
        } else if (lt(o)) {
            return -1;
        } else {
            return 1;
        }
    }
    
    @Override
    public String toUnicode() {
        return toString();
    }
}
