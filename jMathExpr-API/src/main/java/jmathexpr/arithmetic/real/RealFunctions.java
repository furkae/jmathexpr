/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.real;

/**
 *
 * @author Elemér Furka
 */
public interface RealFunctions {
    
    RealNumber exp(RealNumber x);
    
    RealNumber ln(RealNumber x);
    
    RealNumber log(RealNumber base, RealNumber x);
    
    RealNumber sqrt(RealNumber x);
}
