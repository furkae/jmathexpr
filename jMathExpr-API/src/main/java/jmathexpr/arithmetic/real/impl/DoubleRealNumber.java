/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.real.impl;

import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.real.RealNumber;
import jmathexpr.arithmetic.real.Reals;
import jmathexpr.set.Set;

/**
 *
 * @author Elemér Furka
 */
public class DoubleRealNumber extends RealNumber {
    final double value;
    
    public DoubleRealNumber(double value) {
        this.value = value;
    }
    
    @Override
    public DoubleRealNumber add(ANumber addend) {
        if (addend instanceof DoubleRealNumber) {
            return new DoubleRealNumber(value + ((DoubleRealNumber) addend).value);
        } else {
            throw new IllegalArgumentException(addend.getClass() + " Expected: DoubleRealNumber");
        }
    }
    
    @Override
    public DoubleRealNumber subtract(ANumber subtrahend) {
        if (subtrahend instanceof DoubleRealNumber) {
            return new DoubleRealNumber(value - ((DoubleRealNumber) subtrahend).value);
        } else {
            throw new IllegalArgumentException(subtrahend.getClass() + " Expected: DoubleRealNumber");
        }
    }
    
    @Override
    public DoubleRealNumber multiply(ANumber multiplier) {
        if (multiplier instanceof DoubleRealNumber) {
            return new DoubleRealNumber(value * ((DoubleRealNumber) multiplier).value);
        } else {
            throw new IllegalArgumentException(multiplier.getClass() + " Expected: DoubleRealNumber");
        }
    }
    
    @Override
    public DoubleRealNumber divide(ANumber divisor) {
        if (divisor instanceof DoubleRealNumber) {
            DoubleRealNumber other = (DoubleRealNumber) divisor;
            
            if (other.value != 0) {
                return new DoubleRealNumber(value /  other.value);
            } else {
                throw new UnsupportedOperationException(
                        String.format("Division by 0: %s / %s", this, other));
            }
        } else {
            throw new IllegalArgumentException(divisor.getClass() + " Expected: DoubleRealNumber");
        }
    }

    @Override
    public DoubleRealNumber negate() {
        return new DoubleRealNumber(-value);
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (this == object) return true;
        if (object instanceof DoubleRealNumber) {
            return value == ((DoubleRealNumber) object).value;
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.value) ^ (Double.doubleToLongBits(this.value) >>> 32));
        return hash;
    }
    
    @Override
    public String toString() {
        return String.format("%f", value);
    }

    @Override
    public boolean isNegative() {
        return value < 0;
    }

    @Override
    public boolean isZero() {
        return value == 0;
    }

    @Override
    public boolean isOne() {
        return value == 1.0;
    }

    @Override
    public ANumber pow(ANumber exponent) {
        if (exponent instanceof DoubleRealNumber) {
            return new DoubleRealNumber(Math.pow(value, ((DoubleRealNumber) exponent).value));
        } else {
            throw new IllegalArgumentException(exponent.getClass() + " Expected: DoubleRealNumber");
        }
    }

    @Override
    public boolean le(ANumber expr) {
        if (expr instanceof DoubleRealNumber) {
            return value <= ((DoubleRealNumber) expr).value;
        } else {
            throw new IllegalArgumentException(expr.getClass() + " Expected: DoubleRealNumber");
        }
    }

    @Override
    public boolean lt(ANumber expr) {
        if (expr instanceof DoubleRealNumber) {
            return value < ((DoubleRealNumber) expr).value;
        } else {
            throw new IllegalArgumentException(expr.getClass() + " Expected: DoubleRealNumber");
        }
    }
}
