/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.real.impl;

import jmathexpr.arithmetic.real.RealFactory;
import jmathexpr.arithmetic.real.RealNumber;

/**
 *
 * @author Elemér Furka
 */
public class DoubleRealFactory implements RealFactory {

    @Override
    public RealNumber create(String asString) {
        return new DoubleRealNumber(Double.parseDouble(asString));
    }

    @Override
    public RealNumber create(double asDouble) {
        return new DoubleRealNumber(asDouble);
    }    
}
