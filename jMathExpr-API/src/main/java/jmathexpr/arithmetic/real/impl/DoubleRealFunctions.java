/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.real.impl;

import jmathexpr.arithmetic.real.RealFunctions;
import jmathexpr.arithmetic.real.RealNumber;

/**
 *
 * @author Elemér Furka
 */
public class DoubleRealFunctions implements RealFunctions {
    
    @Override
    public RealNumber exp(RealNumber x) {
        DoubleRealNumber rx = (DoubleRealNumber) x.toReal();

        return new DoubleRealNumber(Math.exp(rx.value));
    }

    @Override
    public RealNumber ln(RealNumber x) {
        DoubleRealNumber rx = (DoubleRealNumber) x.toReal();
        
        return new DoubleRealNumber(Math.log(rx.value));
    }

    @Override
    public RealNumber log(RealNumber base, RealNumber x) {
        DoubleRealNumber rbase = (DoubleRealNumber) base.toReal();
        DoubleRealNumber rx = (DoubleRealNumber) x.toReal();
        
        if (!rbase.isZero()) {
            double lnx = Math.log(rx.value);
            double lnb = Math.log(rbase.value);
            
            return new DoubleRealNumber(lnx / lnb);
        } else {
            throw new ArithmeticException(String.format("Undefined expression: log(%s, %s)", base, x));
        }
    }

    @Override
    public RealNumber sqrt(RealNumber x) {
        DoubleRealNumber rx = (DoubleRealNumber) x.toReal();
        
        return new DoubleRealNumber(Math.sqrt(rx.value));
    }
}
