/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.real;

/**
 *
 * @author Elemér Furka
 */
public interface RealFactory {
    RealNumber create(String asString);
    
    RealNumber create(double asDouble);
}
