/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.equation.rule;

import java.util.List;
import jmathexpr.arithmetic.equation.Equation;

/**
 * Indicates that the equation will be converted into a (list of) another type
 * of equations.
 * 
 * @author Elemér Furka
 */
public interface EquationRule {
    
    List<Equation> convertedEquations();
}
