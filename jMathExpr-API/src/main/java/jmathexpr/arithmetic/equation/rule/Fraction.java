/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.equation.rule;

import jmathexpr.Expression;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.func.Lcm;
import jmathexpr.arithmetic.op.Multiplication;
import jmathexpr.arithmetic.pattern.FractionPattern;
import jmathexpr.relation.Equality;
import jmathexpr.util.rule.SimpleRule;

/**
 * Rule for eliminating fractions from an equation.
 * 
 * @author Elemér Furka
 */
public class Fraction extends SimpleRule {
    
    private final java.util.Set<Expression> denominators =
            new java.util.LinkedHashSet();

    private final FractionPattern pattern = new FractionPattern();

    @Override
    public boolean matches(Expression expr) {
        target = expr;

        boolean matches = pattern.matches(expr);

        if (matches) {
            denominators.add(pattern.denominator());
        }

        return matches;
    }

    @Override
    public Expression apply() {
        ANumber lcd = (ANumber) new Lcm(denominators).evaluate();

        return new Equality(new Multiplication(lcd, ((Equality) target).lhs()),
                            new Multiplication(lcd, ((Equality) target).rhs()));
    }

    @Override
    public boolean isRecursive() {
        return true;
    }
}
