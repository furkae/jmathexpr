/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.equation.rule;

import jmathexpr.Expression;
import jmathexpr.Variable;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.relation.Equality;
import jmathexpr.set.FiniteSet;
import jmathexpr.set.Set;
import jmathexpr.util.pattern.TerminationPattern;
import jmathexpr.util.rule.CompositeRule;
import jmathexpr.util.rule.SubRule;

/**
 * The final rule that returns the set of roots of an equation.
 * 
 * @author Elemér Furka
 */
public class LinearTerminationRule extends CompositeRule {
    
    private final TerminationPattern x;
    
    public LinearTerminationRule(Variable x) {
        super(false);
        
        this.x = x;
    }
    
    @Override
    public boolean matches(Expression expr) {
        if ((rule = new XeC()).matches(expr)) return true;
        if ((rule = new AeB()).matches(expr)) return true;

        return false;
    }
    
    /**
     * The case x = c.
     */
    private class XeC extends SubRule {
        
        private final TerminationPattern c = Numbers.constant("c");
        
        @Override
        public boolean matches(Expression expr) {
            return new Equality(x, c).matches(expr);
        }

        @Override
        public Set apply() {
            return new FiniteSet(c.hit());
        }
    }
    
    /**
     * The case a = b (the set of roots is either empty or infinity).
     */
    private class AeB extends SubRule {
        
        private final TerminationPattern a = Numbers.constant("a");
        private final TerminationPattern b = Numbers.constant("b");
        
        @Override
        public boolean matches(Expression expr) {
            return new Equality(a, b).matches(expr);
        }

        @Override
        public Set apply() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
