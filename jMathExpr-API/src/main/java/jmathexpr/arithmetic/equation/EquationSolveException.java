/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.equation;

import java.util.List;

import jmathexpr.relation.Equality;

/**
 * Thrown if an equation cannot be solved.
 * 
 * @author Elemér Furka
 */
public class EquationSolveException extends Exception {
    
    /**
     * Constructs an instance of <code>EquationSolveException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public EquationSolveException(String msg) {
        super(msg);
    }
}
