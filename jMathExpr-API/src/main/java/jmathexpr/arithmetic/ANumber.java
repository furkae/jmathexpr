/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic;

import jmathexpr.Expression;

/**
 * A number.
 * 
 * @author Elemér Furka
 */
public interface ANumber extends Expression, Convertible {
    
    ANumber add(ANumber addend);
    
    ANumber subtract(ANumber subtrahend);
    
    ANumber multiply(ANumber multiplier);

    ANumber divide(ANumber divisor);
    
    ANumber pow(ANumber exponent);

    ANumber negate();
    
    boolean isNatural();
    
    boolean isInteger();
    
    boolean isRational();

    boolean isReal();
    
    boolean isNegative();
    
    boolean isZero();
    
    boolean isOne();
    
    /**
     * Less than or equal to the other number.
     */
    boolean le(ANumber other);

    /**
     * (Strictly) less than the other number.
     */
    boolean lt(ANumber other);    
}
