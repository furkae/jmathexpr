/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.natural;

/**
 *
 * @author Elemér Furka
 */
public interface NaturalFactory {
    
    /**
     * Creates a new NaturalNumber instance parsing the given string value.
     * 
     * @param asString a valid natural number as a string
     * 
     * @return a newly created NaturalNumber instance
     */
    NaturalNumber create(String asString);
    
    /**
     * Creates a new NaturalNumber instance having the given long value.
     * 
     * @param asLong a valid natural number as a long
     * 
     * @return a newly created NaturalNumber instance
     */    
    NaturalNumber create(long asLong);
    
    /**
     * Returns an implementation specific NaturalCalculator instance.
     */
    NaturalCalculator getCalculator();
}
