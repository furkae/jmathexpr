/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.natural;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.arithmetic.op.Exponentiation;
import jmathexpr.op.Sign;
import jmathexpr.set.Set;

/**
 * Decomposition of a natural number into a unique prime factorization. Each
 * factor is a positive integer power of a prime number (p^n).
 * 
 * @author Elemér Furka
 */
public class PrimeFactorization extends AbstractExpression {
    
    private final NaturalNumber n;
    
    private final List<Exponentiation> factors;
    
    /**
     * Creates a new PrimeFactorization instance for an already factorized number.
     * 
     * @param n the natural number
     * @param factors the number's prime factorization as a list
     */
    public PrimeFactorization(NaturalNumber n, List<Exponentiation> factors) {
        this.n = n;
        this.factors = factors;
    }
    
    /**
     * Returns the prime factors in a list. Each prim factor is contained as many
     * times as they are presented in the factorization.
     * 
     * @return a list of prime numbers (e.g. [2, 3, 3, 3] for 54)
     */
    public List<NaturalNumber> primeFactors() {
        List<NaturalNumber> primes = new ArrayList();
        NaturalNumber exp;
        
        for (Exponentiation f : factors) {
            exp = (NaturalNumber) f.exponent();
            
            for (long i = 0; i < exp.longValue(); i++) {
                primes.add((NaturalNumber) f.base());
            }
        }
        
        return primes;
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Multiplication;
    }

    @Override
    public Set domain() {
        return Naturals.getInstance();
    }

    @Override
    public Set codomain() {
        return Naturals.getInstance();
    }

    @Override
    public List<Exponentiation> getChildren() {
        return Collections.unmodifiableList(factors);
    }

    @Override
    public String toUnicode() {
        if (factors.isEmpty()) {
            return n.toUnicode();
        } else if (factors.size() == 1) {
            return factors.get(0).toUnicode();
        }
        
        StringBuilder buf = new StringBuilder();
        
        buf.append(factors.get(0).toUnicode());
        
        for (int i = 1; i < factors.size(); i++) {
            buf.append(" " + Sign.Multiplication.toUnicode() + " " + factors.get(i).toUnicode());
        }
        
        return buf.toString();
    }
    
    @Override
    public String toString() {
        if (factors.isEmpty()) {
            return n.toString();
        } else if (factors.size() == 1) {
            return factors.get(0).toString();
        }
        
        StringBuilder buf = new StringBuilder();
        
        buf.append(factors.get(0));
        
        for (int i = 1; i < factors.size(); i++) {
            buf.append(" " + Sign.Multiplication + " " + factors.get(i));
        }
        
        return buf.toString();
    }
}
