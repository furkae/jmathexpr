/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.natural;

import jmathexpr.arithmetic.integer.IntegerNumber;
import jmathexpr.set.Set;

/**
 * A natural number: 0, 1, 2, ...
 * 
 * @author Elemér Furka
 */
public abstract class NaturalNumber extends IntegerNumber {

    @Override
    public boolean isNatural() {
        return true;
    }

    @Override
    public boolean isNegative() {
        return false;
    }

    @Override
    public Set domain() {
        return Naturals.getInstance();
    }

    @Override
    public Set codomain() {
        return Naturals.getInstance();
    }
    
    /**
     * Returns the prime factorization of this natural number.
     * 
     * @return this number's prime factorization
     */
    public PrimeFactorization factorize() {
        return Naturals.getInstance().getCalculator().factorize(this);
    }
    
    /**
     * Converts this natural number into a Java long value (if it is possible).
     * 
     * @return this natural number as a Java long value
     */
    public abstract long longValue();
}
