/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.natural;

import jmathexpr.Expression;
import jmathexpr.set.Tuple;

/**
 * Common interface of all calculator instances that operate on natural numbers.
 * 
 * @author Elemér Furka
 */
public interface NaturalCalculator {
    
    /**
     * Implementation specific calculation of LCM(args).
     * 
     * @param args the parameters
     * 
     * @return the LCM of the given parameters
     */
    NaturalNumber lcm(Tuple args);
    
    /**
     * Tries to calculate sqrt(n).
     * 
     * @param n a natural number
     * @return sqrt(n) if n is a perfect square (n = a^2) or sqrt(n) otherwise
     */
    Expression sqrt(NaturalNumber n);
    
    /**
     * Tries to calculate log[base](a).
     * 
     * @param base the base of the logarithm
     * @param a a natural number
     * 
     * @return x = log[base](a) if base^x = a and x is a natural number or log[base](a) otherwise
     */
    Expression log(NaturalNumber base, NaturalNumber a);
    
    /**
     * Calculates the prime factorization of the given natural number.
     * 
     * @param n an arbitrary natural number
     * @return the prime factorization of the specified number
     */
    PrimeFactorization factorize(NaturalNumber n);
}
