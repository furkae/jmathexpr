/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.natural.impl;

import jmathexpr.arithmetic.natural.NaturalCalculator;
import jmathexpr.arithmetic.natural.NaturalFactory;
import jmathexpr.arithmetic.natural.NaturalNumber;

/**
 * The trivial NaturalFactory implementation based on natural numbers implemented
 * as Java long values.
 * 
 * @author Elemér Furka
 */
public class LongNaturalFactory implements NaturalFactory {
    
    private final NaturalCalculator calculator = new LongNaturalCalculator();

    @Override
    public NaturalNumber create(String asString) {
        return new LongNaturalNumber(Long.parseLong(asString));
    }

    @Override
    public NaturalNumber create(long asLong) {
        return new LongNaturalNumber(asLong);
    }    

    @Override
    public NaturalCalculator getCalculator() {
        return calculator;
    }
}
