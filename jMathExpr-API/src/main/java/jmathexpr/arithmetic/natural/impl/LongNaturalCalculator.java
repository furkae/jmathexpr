/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.natural.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jmathexpr.Expression;
import jmathexpr.arithmetic.func.Log;
import jmathexpr.arithmetic.func.Sqrt;
import jmathexpr.arithmetic.natural.NaturalCalculator;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.natural.PrimeFactorization;
import jmathexpr.arithmetic.op.Exponentiation;
import jmathexpr.arithmetic.op.Multiplication;
import jmathexpr.set.Tuple;

/**
 * Calculator based on Java long numbers.
 * 
 * @author Elemér Furka
 */
public class LongNaturalCalculator implements NaturalCalculator {
    
    @Override
    public NaturalNumber lcm(Tuple args) {
        if (args.length() == 1) {
            return (LongNaturalNumber) args.at(1);
        }
        
        long result = lcm(((LongNaturalNumber) args.at(1)).value, ((LongNaturalNumber) args.at(2)).value);
        
        for (int i = 3; i <= args.length(); i++) {
            result = lcm(result, ((LongNaturalNumber) args.at(i)).value);
        }
        
        return new LongNaturalNumber(result);
    }
    
    private long lcm(long a, long b) {
        return a * (b / gcd(a, b));
    }
    
    /**
     * Euclid's algorithm.
     */
    private long gcd(long a, long b) {
        long tmp;
        
        while (b > 0) {
            tmp = b;
            b = a % b; // % is remainder
            a = tmp;
        }
        
        return a;
    }

    @Override
    public Expression sqrt(NaturalNumber n) {
        long nvalue = ((LongNaturalNumber) n).value;
        
        if (nvalue == 0 || nvalue == 1) {
            return n;
        }
        
        List<Long> factors = factorize(nvalue);
        long a = 1, s = 1; // n = a^2 * s
        long pp = 0;
        
        for (long p : factors) {
            if (pp == 0) {
                pp = p;
            } else if (p == pp) {
                a *= p;
                pp = 0;
            } else {
                s *= pp;
                pp = p;
            }
        }
        
        if (pp != 0) {
            s *= pp;
        }
        
        if (s == 1) { // perfect square
            return new LongNaturalNumber(a);
        } else if (a == 1) {
            return new Sqrt(n);
        } else { // sqrt(n) = a * sqrt(s)
            return new Multiplication(new LongNaturalNumber(a), new Sqrt(new LongNaturalNumber(s)));
        }
    }
    
    private List<Long> factorize(long number) {
        long n = number;
        long limit = (long) Math.ceil(Math.sqrt(n));
        List<Long> factors = new ArrayList();
        Iterable<Long> primes = new Primes().getPrimes(limit);
        
        for (long p : primes) {
            while (n % p == 0) {
                factors.add(p);
                n /= p;
            }
        }
        
        if (n != 1) {
            factors.add(n);
        }
        
        return factors;
    }

    @Override
    public Expression log(NaturalNumber base, NaturalNumber a) {
        if (!base.isZero() && !base.isNegative()) {
            LongNaturalNumber guess = log((LongNaturalNumber) base, (LongNaturalNumber) a);
            
            if (base.pow(guess).equals(a)) {
                return guess;
            } else {
                return new Log(base, a);
            }
        } else {
            throw new ArithmeticException(String.format("Undefined expression: log[%s](%s)", base, a));
        }
    }
    
    private static LongNaturalNumber log(LongNaturalNumber base, LongNaturalNumber a) {
        double lna = Math.log(a.value);
        double lnb = Math.log(base.value);

        return new LongNaturalNumber(Math.round(lna / lnb));
    }

    @Override
    public PrimeFactorization factorize(NaturalNumber n) {
        List<Long> factors = factorize(((LongNaturalNumber) n).value);
        Map<Long, Long> powers = new HashMap();
        
        for (long p : factors) {
            if (powers.containsKey(p)) {
                powers.put(p, powers.get(p) + 1);
            } else {
                powers.put(p, 1L);
            }
        }
        
        List<Exponentiation> explist = new ArrayList();
        
        for (long p : powers.keySet()) {
            explist.add(new Exponentiation(
                    new LongNaturalNumber(p), new LongNaturalNumber(powers.get(p))));
        }
        
        return new PrimeFactorization(n, explist);
    }
}
