/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.natural.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * This class implements all prime related methods.
 * 
 * @author Elemér Furka
 */
public class Primes {
    
    private static final List<Long> PRIMES = new ArrayList();
    
    private static final String PRIMES_FILE = "primes_10000.txt";
    
    static {
//        try {
            InputStream stream = Primes.class.getClassLoader().getResourceAsStream(PRIMES_FILE);
            Scanner sc = new Scanner(stream);
            
            while (sc.hasNextLong()) {
                PRIMES.add(sc.nextLong());
            }
//        } catch (IOException e) {
//            System.err.println(e);
//            
//            System.exit(-1);
//        }
    }

    Iterable<Long> getPrimes(long max) {
        return new FirstPrimes(max);
    }
    
    private class FirstPrimes implements Iterable<Long>, Iterator<Long> {
        
        private final long max;
        
        private int idx = 0;
        
        private FirstPrimes(long max) {
            if (max > PRIMES.get(PRIMES.size() - 1)) {
                throw new IllegalStateException(String.format(
                        "List of primes is too short: %s < %s", PRIMES.get(PRIMES.size() - 1), max));
            }
            
            this.max = max;
        }

        @Override
        public boolean hasNext() {
            return idx < PRIMES.size() && PRIMES.get(idx) <= max;
        }

        @Override
        public Long next() {
            return PRIMES.get(idx++);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Iterator<Long> iterator() {
            return this;
        }
    }
}
