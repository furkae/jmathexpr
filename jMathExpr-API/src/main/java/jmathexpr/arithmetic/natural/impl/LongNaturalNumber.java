/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.natural.impl;

import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.integer.IntegerNumber;
import jmathexpr.arithmetic.integer.impl.LongIntegerNumber;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.rational.RationalNumber;
import jmathexpr.arithmetic.rational.impl.LongRationalNumber;
import jmathexpr.arithmetic.real.RealNumber;
import jmathexpr.arithmetic.real.impl.DoubleRealNumber;

/**
 *
 * @author Elemér Furka
 */
public class LongNaturalNumber extends NaturalNumber {

    final long value;

    public LongNaturalNumber(long value) {
        if (value < 0) {
            throw new IllegalArgumentException("Non-negative number expected: " + value);
        }

        this.value = value;
    }

    @Override
    public LongNaturalNumber evaluate() {
        return this;
    }

    @Override
    public ANumber add(ANumber addend) {
        if (addend instanceof LongNaturalNumber) {
            return new LongNaturalNumber(value + ((LongNaturalNumber) addend).value);
        } else {
            return Numbers.add(this, addend);
        }
    }

    @Override
    public ANumber multiply(ANumber multiplier) {
        if (multiplier instanceof LongNaturalNumber) {
            return new LongNaturalNumber(value * ((LongNaturalNumber) multiplier).value);
        } else {
            return Numbers.multiply(this, multiplier);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this == object) {
            return true;
        }
        if (object instanceof LongNaturalNumber) {
            LongNaturalNumber other = (LongNaturalNumber) object;

            return value == other.value;
        } else if (object instanceof ANumber) {
            return Numbers.equal(this, (ANumber) object);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (int) (this.value ^ (this.value >>> 32));
        return hash;
    }

    @Override
    public String toString() {
        return Long.toString(value);
    }

    @Override
    public IntegerNumber negate() {
        return new LongIntegerNumber(-value);
    }

    @Override
    public ANumber subtract(ANumber subtrahend) {
        if (subtrahend instanceof LongNaturalNumber) {
            LongNaturalNumber rhs = (LongNaturalNumber) subtrahend;

            if (value >= rhs.value) {
                return new LongNaturalNumber(value - rhs.value);
            } else {
                return new LongIntegerNumber(value - rhs.value);
            }
        } else {
            return Numbers.subtract(this, subtrahend);
        }
    }

    @Override
    public boolean isZero() {
        return value == 0;
    }

    @Override
    public ANumber divide(ANumber divisor) {
        if (divisor instanceof LongNaturalNumber) {
            LongNaturalNumber rhs = (LongNaturalNumber) divisor;

            if (value % rhs.value == 0) {
                return new LongNaturalNumber(value / rhs.value);
            } else {
                return new LongRationalNumber(value, rhs.value);
            }
        } else {
            return Numbers.divide(this, divisor);
        }
    }

    @Override
    public IntegerNumber mod(NaturalNumber modulus) {
        return new LongIntegerNumber(value % ((LongNaturalNumber) modulus).value);
    }

    @Override
    public boolean isOne() {
        return value == 1;
    }

    @Override
    public ANumber pow(ANumber exponent) {
        return toInteger().pow(exponent);
    }

    @Override
    public RealNumber toReal() {
        return new DoubleRealNumber(value);
    }

    @Override
    public boolean le(ANumber expr) {
        if (expr instanceof LongNaturalNumber) {
            return value <= ((LongNaturalNumber) expr).value;
        } else {
            throw new IllegalArgumentException(expr.getClass() + " Expected: LongNaturalNumber");
        }
    }

    @Override
    public boolean lt(ANumber expr) {
        if (expr instanceof LongNaturalNumber) {
            return value < ((LongNaturalNumber) expr).value;
        } else {
            return Numbers.lt(this, expr);
        }
    }

    @Override
    public RationalNumber toRational() {
        return new LongRationalNumber(value, 1);
    }

    @Override
    public IntegerNumber toInteger() {
        return new LongIntegerNumber(value);
    }

    @Override
    public long longValue() {
        return value;
    }
}
