/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.natural;

import jmathexpr.Expression;
import jmathexpr.bool.TruthValue;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.integer.Integers;
import jmathexpr.arithmetic.natural.impl.LongNaturalFactory;
import jmathexpr.arithmetic.rational.Rationals;
import jmathexpr.arithmetic.real.Reals;
import jmathexpr.set.Cardinality;
import jmathexpr.set.Set;
import jmathexpr.set.Tuple;

/**
 * The set of all natural numbers.
 * 
 * @author Elemér Furka
 */
public class Naturals extends Numbers implements NaturalFactory {
    
    private static final Naturals INSTANCE = new Naturals();
    
    private final NaturalFactory factory;
    
    private Naturals() {
        this(new LongNaturalFactory());
    }
    
    private Naturals(NaturalFactory factory) {
        super ('N', "\u2115");
        
        this.factory = factory;
    }
    
    public static Naturals getInstance() {
        return INSTANCE;
    }
    
    public static Naturals getInstance(NaturalFactory factory) {
        return new Naturals(factory);
    }
    
    @Override
    public Naturals evaluate() {
        return this;
    }

    @Override
    public TruthValue contains(Expression element) {
        return element instanceof ANumber ? TruthValue.valueOf(((ANumber) element).isNatural()) : TruthValue.False;
    }

    @Override
    public boolean subsetOf(Set set) {
        if (equals(set)) {
            return true;
        } else if (set.equals(Reals.getInstance()) || set.equals(Rationals.getInstance())
                || set.equals(Integers.getInstance())) {
            return true;
        }
        
        throw new UnsupportedOperationException(String.format(
                "Not supported yet: %s (%s)", set, set.getClass()));
    }

    @Override
    public NaturalNumber create(String asString) {
        return factory.create(asString);
    }

    @Override
    public NaturalNumber create(long asLong) {
        return factory.create(asLong);
    }
    
    /**
     * Convenience method for Naturals#getInstance()#create(0).
     */
    public static NaturalNumber zero() {
        return getInstance().create(0);
    }
    
    /**
     * Convenience method for Naturals#getInstance()#create(1).
     */
    public static NaturalNumber one() {
        return getInstance().create(1);
    }
    
    /**
     * Convenience method for NaturalCalculator#lcm(Tuple).
     */
    public static NaturalNumber lcm(Tuple args) {
        return getInstance().getCalculator().lcm(args);
    }
    
    /**
     * Convenience method for NaturalCalculator#sqrt(NaturalNumber).
     */
    public static Expression sqrt(NaturalNumber n) {
        return getInstance().getCalculator().sqrt(n);
    }
    
    /**
     * Convenience method for NaturalCalculator#log(NaturalNumber,NaturalNumber).
     */
    public static Expression log(NaturalNumber base, NaturalNumber a) {
        return getInstance().getCalculator().log(base, a);
    }

    @Override
    public NaturalCalculator getCalculator() {
        return factory.getCalculator();
    }

    @Override
    public Set domain() {
        return this;
    }

    @Override
    public Set codomain() {
        return this;
    }

    @Override
    public Cardinality cardinality() {
        return Cardinality.aleph(0);
    }
}
