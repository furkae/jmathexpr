/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.func;

import jmathexpr.Expression;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.real.RealNumber;
import jmathexpr.arithmetic.real.Reals;

/**
 * The exponential function exp(x).
 * 
 * @author Elemér Furka
 */
public class Exp extends UnivariateNumberFunction {
    
    public Exp(Expression arg) {
        super("exp", arg);
    }

    @Override
    public Expression evaluate() {
        Expression x = arg.evaluate();
        
        if (x instanceof ANumber) {
            if (((ANumber) x).isZero()) {
                return Naturals.one();
            } else if (x instanceof RealNumber) {
                return Reals.getInstance().functions().exp((RealNumber) x);
            }
        }

        return new Exp(x);
    }

    @Override
    protected UnivariateNumberFunction create(Expression arg) {
        return new Exp(arg);
    }
}
