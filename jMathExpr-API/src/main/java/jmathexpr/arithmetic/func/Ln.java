/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.func;

import jmathexpr.Expression;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.real.RealNumber;
import jmathexpr.arithmetic.real.Reals;

/**
 * The natural logarithm function: ln(x).
 * 
 * @author Elemér Furka
 */
public class Ln extends UnivariateNumberFunction {
    
    public Ln(Expression arg) {
        super("ln", arg);
    }

    @Override
    public Expression evaluate() {
        Expression x = arg.evaluate();
        
        if (x instanceof ANumber) {
            if (x instanceof RealNumber) {
                return Reals.getInstance().functions().ln((RealNumber) x);
            }
        }

        return new Ln(x);
    }

    @Override
    protected UnivariateNumberFunction create(Expression arg) {
        return new Ln(arg);
    }
}
