/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.func;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.func.Function;
import jmathexpr.arithmetic.real.Reals;
import jmathexpr.set.Set;
import jmathexpr.util.logging.Logger;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.rule.Rule;

/**
 * This abstract class represents a univariate number function: a function that only
 * has one variable and whose codomain is a set of numbers.
 * 
 * @author Elemér Furka
 */
public abstract class UnivariateNumberFunction extends AbstractExpression
        implements Function, ExpressionPattern {
    
    /**
     * The name of this function.
     */
    protected final String name;
    
    /**
     * The argument of this function. It can be any expression that evaluates to
     * a number.
     */
    protected final Expression arg;
    
    protected UnivariateNumberFunction(String name, Expression arg) {
        this.name = name;
        this.arg = arg;
    }
    
    protected abstract UnivariateNumberFunction create(Expression arg);
    
    @Override
    public Set domain() {
        return Reals.getInstance();
    }

    @Override
    public Set codomain() {
        return Reals.getInstance();
    }
    
    /**
     * Returns the argument of this function.
     * 
     * @return the argument of this function
     */
    public Expression argument() {
        return arg;
    }

    @Override
    public List<Expression> getChildren() {
        return Collections.singletonList(arg);
    }
    
    @Override
    public String toString() {
        return String.format("%s(%s)", name, arg);
    }
    
    @Override
    public String toUnicode() {
        return toString();
    }
    
    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (this == object) return true;
        if (object instanceof UnivariateNumberFunction && getClass().equals(object.getClass())) {
            return arg.equals(((UnivariateNumberFunction) object).arg);
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.arg);
        return hash;
    }

    @Override
    public boolean isConstant() {
        return arg.isConstant();
    }
    
    @Override
    public boolean contains(ExpressionPattern pattern) {
        if (getClass().isAssignableFrom(pattern.getClass())) {
            UnivariateNumberFunction p = (UnivariateNumberFunction) pattern;
            
            if (arg.contains((ExpressionPattern) p.arg)) {
                return true;
            }
        }
        
        return arg.contains(pattern);
    }

    @Override
    public boolean isApplicable(Rule rule) {
        if (rule.matches(this)) {
            return true;
        } else if (arg.isApplicable(rule)) {
            rule.register(create(rule.apply()));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Function;
    }

    @Override
    public boolean matches(Expression expr) {
        if (expr.getClass().isAssignableFrom(getClass())) {
            UnivariateNumberFunction other = (UnivariateNumberFunction) expr;
            
            if (((ExpressionPattern) arg).matches(other.arg)) {
                return true;
            }
        }
        
        return false;
    }
}
