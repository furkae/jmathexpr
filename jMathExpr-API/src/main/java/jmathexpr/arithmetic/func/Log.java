/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.func;

import java.util.Objects;
import jmathexpr.Expression;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.real.RealNumber;
import jmathexpr.arithmetic.real.Reals;

/**
 * The logarithm function: log[base](x).
 * 
 * @author Elemér Furka
 */
public class Log extends UnivariateNumberFunction {
    
    private final Expression base;
    
    public Log(Expression base, Expression arg) {
        super("log", arg);
        
        this.base = base;
    }

    @Override
    public Expression evaluate() {
        Expression b = base.evaluate();
        Expression x = arg.evaluate();
        
        if (b instanceof ANumber && x instanceof ANumber) {
            if (b instanceof NaturalNumber && x instanceof NaturalNumber) {
                return Naturals.log((NaturalNumber) b, (NaturalNumber) x);
            } else if (b instanceof RealNumber && x instanceof RealNumber) {
                return Reals.getInstance().functions().log((RealNumber) b, (RealNumber) x);
            }
        }

        return new Log(b, x);
    }
    
    @Override
    public String toString() {
        return String.format("%s[%s](%s)", name, base, arg);
    }
    
    @Override
    public boolean equals(Object object) {
        return super.equals(object) && base.equals(((Log) object).base);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 61 * hash + Objects.hashCode(this.base);
        return hash;
    }
    
    @Override
    public boolean matches(Expression expr) {
        return super.matches(expr) && base.equals(((Log) expr).base);
    }

    @Override
    protected UnivariateNumberFunction create(Expression arg) {
        return new Log(base, arg);
    }
}
