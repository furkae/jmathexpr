/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.rational;

import jmathexpr.Precedence;
import jmathexpr.arithmetic.integer.IntegerNumber;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.real.RealNumber;
import jmathexpr.set.Set;

/**
 * A rational number.
 * 
 * @author Elemér Furka
 */
public abstract class RationalNumber extends RealNumber {

    @Override
    public boolean isRational() {
        return true;
    }

    @Override
    public RationalNumber toRational() {
        return this;
    }

    @Override
    public IntegerNumber toInteger() {
        throw new UnsupportedOperationException("Cannot convert a Rational to an Integer");
    }
    
    
    /**
     * Returns the numerator of this rational number.
     * 
     * @return the signed (integer) numerator
     */
    public abstract IntegerNumber numerator();
    
    /**
     * Returns the denominator of this rational number.
     * 
     * @return the denominator as a natural number
     */
    public abstract NaturalNumber denominator();

    @Override
    public Precedence getPrecedence() {
        if (!denominator().isOne()) {
            return Precedence.Multiplication; // to correctly put into parenthesis
        } else {
            return super.getPrecedence();
        }
    }

    @Override
    public Set domain() {
        return Rationals.getInstance();
    }

    @Override
    public Set codomain() {
        return Rationals.getInstance();
    }
}
