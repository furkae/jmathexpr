/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.rational.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jmathexpr.arithmetic.rational.RationalFactory;
import jmathexpr.arithmetic.rational.RationalNumber;

/**
 *
 * @author Elemér Furka
 */
public class LongRationalFactory implements RationalFactory {
    
    private static final Pattern DECIMAL_PATTERN = Pattern.compile("(\\+|-)?(\\d*)\\.(\\d+)");

    /**
     * Parses decimal notation strings (e.g. 3.75).
     * 
     * @param asString input string using decimal notation
     * @return a newly created RationalNumber instance
     */
    @Override
    public RationalNumber create(String decimalNumber) {
        Matcher matcher = DECIMAL_PATTERN.matcher(decimalNumber);
        
        if (matcher.matches()) {
            long minus = "-".equals(matcher.group(1)) ? -1L : 1L;
            long integerPart = matcher.group(2) != null ? Long.parseLong(matcher.group(2)) : 0L;
            String sFractionalPart = matcher.group(3);
            long fractionalPart = sFractionalPart != null ? Long.parseLong(sFractionalPart) : 0L;
            long denominator = 1;
            
            if (sFractionalPart != null) {
                for (int i = 0; i < sFractionalPart.length(); i++) {
                    denominator *= 10L;
                }
            }
            
            return new LongRationalNumber(minus * (integerPart * denominator + fractionalPart), denominator);
        } 
        
        throw new IllegalArgumentException("Not a decimal number: " + decimalNumber);
    }

    @Override
    public RationalNumber create(String numerator, String denominator) {
        return new LongRationalNumber(Long.parseLong(numerator), Long.parseLong(denominator));
    }

    @Override
    public RationalNumber create(long numerator, long denominator) {
        return new LongRationalNumber(numerator, denominator);
    }
}
