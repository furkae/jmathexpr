/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.rational;

import jmathexpr.Expression;
import jmathexpr.bool.TruthValue;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.integer.Integers;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.rational.impl.LongRationalFactory;
import jmathexpr.arithmetic.real.Reals;
import jmathexpr.set.Cardinality;
import jmathexpr.set.Set;

/**
 * The set of all rational numbers.
 * 
 * @author Elemér Furka
 */
public class Rationals extends Numbers implements RationalFactory {
    
    private static final Rationals INSTANCE = new Rationals();
    
    private final RationalFactory factory;
    
    private Rationals() {
        this(new LongRationalFactory());
    }
    
    private Rationals(RationalFactory factory) {
        super('Q', "\u2124");
        
        this.factory = factory;
    }
    
    public static Rationals getInstance() {
        return INSTANCE;
    }
    
    public static Rationals getInstance(RationalFactory factory) {
        return new Rationals(factory);
    }
    
    @Override
    public Rationals evaluate() {
        return this;
    }

    @Override
    public TruthValue contains(Expression element) {
        return element instanceof ANumber ? TruthValue.valueOf(((ANumber) element).isRational()) : TruthValue.False;
    }

    @Override
    public boolean subsetOf(Set set) {
        if (equals(set)) {
            return true;
        } else if (set.equals(Reals.getInstance())) {
            return true;
        } else if (set.equals(Naturals.getInstance()) || set.equals(Integers.getInstance())) {
            return false;
        }
        
        throw new IllegalStateException("Missing implementation: " + set);
    }

    @Override
    public RationalNumber create(String asString) {
        return factory.create(asString);
    }

    @Override
    public RationalNumber create(String numerator, String denominator) {
        return factory.create(numerator, denominator);
    }

    @Override
    public RationalNumber create(long numerator, long denominator) {
        return factory.create(numerator, denominator);
    }

    @Override
    public Set domain() {
        return this;
    }

    @Override
    public Set codomain() {
        return this;
    }

    @Override
    public Cardinality cardinality() {
        return Cardinality.aleph(0);
    }
}
