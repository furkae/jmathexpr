/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.rational;

/**
 *
 * @author Elemér Furka
 */
public interface RationalFactory {
    RationalNumber create(String asString);
    
    RationalNumber create(String numerator, String denominator);    
    
    RationalNumber create(long numerator, long denominator);
}
