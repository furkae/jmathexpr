/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.relation;

import jmathexpr.Expression;
import jmathexpr.bool.TruthValue;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.real.Infinity;
import jmathexpr.relation.RelationSymbol;
import jmathexpr.relation.order.TotalOrder;
import jmathexpr.set.OrderedPair;

/**
 * Binary relation Less than or Equal.
 * 
 * @author Elemér Furka
 */
public class LE extends TotalOrder {

    public LE(Expression lhs, Expression rhs) {
        super(lhs, rhs, RelationSymbol.LE);
    }
    
    @Override
    public Expression evaluate() {
        Expression l = lhs.evaluate();
        Expression r = rhs.evaluate();
        
        if (l instanceof ANumber && r instanceof ANumber) {
            OrderedPair args = Numbers.toSameType(l, r);
            
            return TruthValue.valueOf(((ANumber) args.a()).le((ANumber) args.b()));
        } else if (l.equals(Infinity.MINUS_INFINITY) &&
                ((r instanceof Infinity) || (r instanceof ANumber))) {
            return TruthValue.True;
        } else if (r.equals(Infinity.PLUS_INFINITY) &&
                ((l instanceof Infinity) || (l instanceof ANumber))) {
            return TruthValue.True;
        } else if (l.equals(Infinity.MINUS_INFINITY) && r.equals(Infinity.PLUS_INFINITY)) {
            return TruthValue.True;
        }

        return new LE(l, r);
    }

    @Override
    protected LE create(Expression lhs, Expression rhs) {
        return new LE(lhs, rhs);
    }

    @Override
    public boolean isSymmetric() {
        return false;
    }
}
