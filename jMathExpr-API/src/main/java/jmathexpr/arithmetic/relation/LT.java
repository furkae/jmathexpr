/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.relation;

import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.bool.TruthValue;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.real.Infinity;
import jmathexpr.relation.BinaryRelation;
import jmathexpr.relation.RelationSymbol;
import jmathexpr.set.OrderedPair;

/**
 * Binary relation Less than.
 * 
 * @author Elemér Furka
 */
public class LT extends BinaryRelation {

    public LT(Expression lhs, Expression rhs) {
        super(lhs, rhs, RelationSymbol.LT);
    }
    
    @Override
    public Expression evaluate() {
        Expression l = lhs.evaluate();
        Expression r = rhs.evaluate();
        
        if (l instanceof ANumber && r instanceof ANumber) {
            OrderedPair args = Numbers.toSameType(l, r);
            
            return TruthValue.valueOf(((ANumber) args.a()).lt((ANumber) args.b()));
        } else if (l.equals(Infinity.MINUS_INFINITY) && (r instanceof ANumber)) {
            return TruthValue.True;
        } else if (r.equals(Infinity.PLUS_INFINITY) && (l instanceof ANumber)) {
            return TruthValue.True;
        } else if (l.equals(Infinity.MINUS_INFINITY) && r.equals(Infinity.PLUS_INFINITY)) {
            return TruthValue.True;
        }

        return new LT(l, r);
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Comparison;
    }

    @Override
    protected LT create(Expression lhs, Expression rhs) {
        return new LT(lhs, rhs);
    }

    @Override
    public boolean isSymmetric() {
        return false;
    }
}
