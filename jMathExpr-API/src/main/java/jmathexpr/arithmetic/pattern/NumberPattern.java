/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.arithmetic.pattern;

import jmathexpr.Expression;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.util.pattern.AbstractPattern;

/**
 * This class represents a number in an expression pattern.
 * 
 * @author Elemér Furka
 */
public class NumberPattern extends AbstractPattern {
    
    @Override
    public boolean matches(Expression expr) {
        if (expr instanceof ANumber) {
            value = (ANumber) expr;
            
            return true;
        } else {
            return false;
        }
    }

    @Override
    public ANumber hit() {
        return (ANumber) value;
    }

    @Override
    public String toString() {
        return "<n>";
    }
}
