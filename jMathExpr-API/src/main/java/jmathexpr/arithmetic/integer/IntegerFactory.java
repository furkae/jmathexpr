/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.integer;

/**
 *
 * @author Elemér Furka
 */
public interface IntegerFactory {
    IntegerNumber create(String asString);
    
    IntegerNumber create(long asLong);
}
