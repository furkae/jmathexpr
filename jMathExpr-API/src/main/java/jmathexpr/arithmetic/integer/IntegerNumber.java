/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.integer;

import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.rational.RationalNumber;
import jmathexpr.set.Set;

/**
 * Abstract base class of all integer number implementations.
 * 
 * @author Elemér Furka
 */
public abstract class IntegerNumber extends RationalNumber {

    @Override
    public boolean isInteger() {
        return true;
    }

    @Override
    public IntegerNumber toInteger() {
        return this;
    }
    
    @Override
    public IntegerNumber numerator() {
        return this;
    }
    
    @Override
    public NaturalNumber denominator() {
        return Naturals.one();
    }
    
    /**
     * Calculates this mod modulus. I.e. 25 mod 7 = 4 (as 25 - 3*7 = 4).
     * 
     * @param modulus the modulus
     * @return this mod modulus
     */
    public abstract IntegerNumber mod(NaturalNumber modulus);

    @Override
    public Set domain() {
        return Integers.getInstance();
    }

    @Override
    public Set codomain() {
        return Integers.getInstance();
    }
}
