/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.integer;

import jmathexpr.Expression;
import jmathexpr.bool.TruthValue;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.integer.impl.LongIntegerFactory;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.rational.Rationals;
import jmathexpr.arithmetic.real.Reals;
import jmathexpr.set.Cardinality;
import jmathexpr.set.Set;

/**
 * The set of all integer numbers.
 * 
 * @author Elemér Furka
 */
public class Integers extends Numbers implements IntegerFactory {
   
    private static final Integers INSTANCE = new Integers();
    
    private final IntegerFactory factory;
    
    private Integers() {
        this(new LongIntegerFactory());
    }
    
    private Integers(IntegerFactory factory) {
        super('Z', "\u2124");

        this.factory = factory;
    }
    
    public static Integers getInstance() {
        return INSTANCE;
    }
    
    public static Integers getInstance(IntegerFactory factory) {
        return new Integers(factory);
    }
    
    @Override
    public Integers evaluate() {
        return this;
    }

    @Override
    public TruthValue contains(Expression element) {
        return element instanceof ANumber ? TruthValue.valueOf(((ANumber) element).isInteger()) : TruthValue.False;
    }

    @Override
    public boolean subsetOf(Set set) {
        if (equals(set)) {
            return true;
        } else if (set.equals(Reals.getInstance()) || set.equals(Rationals.getInstance())) {
            return true;
        } else if (set.equals(Naturals.getInstance())) {
            return false;
        }
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IntegerNumber create(String asString) {
        return factory.create(asString);
    }

    @Override
    public IntegerNumber create(long asLong) {
        return factory.create(asLong);
    }

    @Override
    public Set domain() {
        return this;
    }

    @Override
    public Set codomain() {
        return this;
    }

    @Override
    public Cardinality cardinality() {
        return Cardinality.aleph(0);
    }
}
