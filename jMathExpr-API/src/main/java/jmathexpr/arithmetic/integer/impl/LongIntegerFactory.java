/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic.integer.impl;

import jmathexpr.arithmetic.integer.IntegerFactory;
import jmathexpr.arithmetic.integer.IntegerNumber;

/**
 *
 * @author Elemér Furka
 */
public class LongIntegerFactory implements IntegerFactory {

    @Override
    public IntegerNumber create(String asString) {
        return new LongIntegerNumber(Long.parseLong(asString));
    }

    @Override
    public IntegerNumber create(long asLong) {
        return new LongIntegerNumber(asLong);
    }    
}
