/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.arithmetic;

import jmathexpr.arithmetic.integer.IntegerNumber;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.rational.RationalNumber;
import jmathexpr.arithmetic.real.RealNumber;

/**
 * Number type conversion methods.
 * 
 * @author Elemér Furka
 */
public interface Convertible {
    
    /**
     * Converts into a real number.
     * 
     * @return a RealNumber instance
     */
    RealNumber toReal();
    
    /**
     * Converts into a rational number.
     * 
     * @return a RationalNumber instance
     */
    RationalNumber toRational();
    
    /**
     * Converts into an integer.
     * 
     * @return an IntegerNumber instance
     */
    IntegerNumber toInteger();
    
    /**
     * Converts into a natural number.
     * 
     * @return a NaturalNumber instance
     */
    NaturalNumber toNatural();
}
