/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.set.op;

import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.bool.TruthValue;
import jmathexpr.op.BinaryOperation;
import jmathexpr.op.Sign;
import jmathexpr.set.Cardinality;
import jmathexpr.set.EmptySet;
import jmathexpr.set.FiniteSet;
import jmathexpr.set.Set;

/**
 * Difference of two sets: all elements that are contained in the first set but
 * that are not elements of the second set.
 * 
 * @author Elemér Furka
 */
public class Difference extends BinaryOperation<Set, Set> implements Set {
    
    public Difference(Set a, Set b) {
        super (a, b, Sign.Difference);
    }

    @Override
    public TruthValue contains(Expression element) {
        return ((Set) lhs).contains(element).and(((Set) rhs).contains(element).not());
    }

    @Override
    public boolean subsetOf(Set set) {
        if (lhs.subsetOf(set)) {
            return true;
        } else {
            throw new IllegalStateException("Missing implementation: " + set);
        }
    }

    @Override
    public Set evaluate() {
        return this;
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Addition;
    }

    @Override
    public Set domain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set codomain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Difference create(Expression lhs, Expression rhs) {
        return new Difference((Set) lhs, (Set) rhs);
    }

    @Override
    public Set union(Set set) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isCommutative() {
        return false;
    }

    @Override
    public Cardinality cardinality() {
        if (lhs instanceof EmptySet) {
            return new Cardinality(Naturals.zero());
        } else if (rhs instanceof EmptySet) {
            return lhs.cardinality();
        }
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
