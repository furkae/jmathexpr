/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.set.op;

import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.bool.TruthValue;
import jmathexpr.op.BinaryOperation;
import jmathexpr.op.Operation;
import jmathexpr.op.Sign;
import jmathexpr.set.Cardinality;
import jmathexpr.set.Set;
import jmathexpr.set.Tuple;

/**
 * Cartesian product of two sets.
 * 
 * @author Elemér Furka
 */
public class CartesianProduct extends BinaryOperation<Set, Set> implements Set {
    
    public CartesianProduct(Set a, Set b) {
        super(a, b, Sign.Cross);
    }

    @Override
    public TruthValue contains(Expression element) {
        if (element instanceof Tuple) {
            Tuple tuple = (Tuple) element;
            
            return TruthValue.valueOf(tuple.length() == 2 && tuple.at(1).equals(lhs) && tuple.at(2).equals(rhs));
        } else {
            return TruthValue.False;
        }
    }

    @Override
    public boolean subsetOf(Set set) {
        if (set instanceof CartesianProduct) {
            return lhs.subsetOf(((CartesianProduct) set).lhs) && rhs.subsetOf(((CartesianProduct) set).rhs);
        } else {
            throw new IllegalStateException("Missing implementation: " + set);
        }
    }

    @Override
    public Set evaluate() {
        return new CartesianProduct(lhs.evaluate(), rhs.evaluate());
    }
    
    @Override
    public Precedence getPrecedence() {
        return Precedence.Multiplication;
    }

    @Override
    public Set domain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set codomain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Operation create(Expression lhs, Expression rhs) {
        return new CartesianProduct((Set) lhs, (Set) rhs);
    }

    @Override
    public Set union(Set set) {
        if (set.subsetOf(this)) {
            return this;
        } else if (subsetOf(set)) {
            return set;
        } else {
            throw new IllegalStateException("Missing implementation: " + set);
        }
    }

    @Override
    public boolean isCommutative() {
        return false;
    }

    @Override
    public Cardinality cardinality() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
