/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.set;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.set.op.CartesianProduct;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.rule.Rule;

/**
 * Ordered pair: (a, b). Element of a cartesian product.
 * 
 * @author Elemér Furka
 */
public class OrderedPair extends AbstractExpression implements Tuple {
    
    private final Expression a;
    
    private final Expression b;
    
    public OrderedPair(Expression a, Expression b) {
        this.a = a;
        this.b = b;
    }
    
    public Expression a() {
        return a;
    }
    
    public Expression b() {
        return b;
    }

    @Override
    public int length() {
        return 2;
    }

    @Override
    public Expression evaluate() {
        return new OrderedPair(a.evaluate(), b.evaluate());
    }    

    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (this == object) return true;
        if (object instanceof OrderedPair) {
            OrderedPair other = (OrderedPair) object;
            
            return a.equals(other.a) && b.equals(other.b);
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.a);
        hash = 19 * hash + Objects.hashCode(this.b);
        return hash;
    }
    
    @Override
    public String toString() {
        return String.format("(%s, %s)", a, b);
    }
    
    @Override
    public String toUnicode() {
        return toString();
    }

    @Override
    public Expression at(int index) {
        if (index == 1) {
            return a;
        } else if (index == 2) {
            return b;
        } else {
            throw new IllegalArgumentException("Illegal tuple index: " + index);
        }
    }

    @Override
    public boolean isConstant() {
        return a.isConstant() && b.isConstant();
    }

    @Override
    public boolean contains(ExpressionPattern pattern) {
        if (pattern instanceof OrderedPair) {
            OrderedPair p = (OrderedPair) pattern;
            
            return a.contains((ExpressionPattern) p.a) && b.contains((ExpressionPattern) p.b);
        }
        
        return false;
    }

    @Override
    public boolean isApplicable(Rule rule) {
        if (rule.matches(this)) {
            return true;
        }
        
        boolean isApplicable = false;
        Expression aa = a, bb = b;
        
        if (a.isApplicable(rule)) {
            isApplicable = true;
            aa = rule.subapply();
        }
        if (b.isApplicable(rule)) {
            isApplicable = true;
            bb = rule.subapply();
        }
        
        if (isApplicable) {
            rule.register(new OrderedPair(aa, bb));
        }
        
        return isApplicable;
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }

    @Override
    public Set domain() {
        return new CartesianProduct(a.domain(), b.domain());
    }

    @Override
    public Set codomain() {
        return new CartesianProduct(a.codomain(), b.codomain());
    }

    @Override
    public List<Expression> getChildren() {
        List<Expression> children = new ArrayList();
        
        children.add(a);
        children.add(b);
        
        return Collections.unmodifiableList(children);
    }
}
