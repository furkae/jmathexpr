/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.set;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;

/**
 * N-tuple (ordered list of n expressions).
 * 
 * @author Elemér Furka
 */
public class NTuple extends AbstractExpression implements Tuple {
    
    private final Expression[] tuple;
    
    /**
     * Creates a tuple having the specified elements in the given collection.
     * 
     * @param elements list of expressions
     */
    public <E extends Expression> NTuple(Collection<E> elements) {
        tuple = elements.toArray(new Expression[elements.size()]);
    }
    
    private NTuple(Expression[] elements) {
        this.tuple = elements;
    }

    @Override
    public int length() {
        return tuple.length;
    }

    @Override
    public Expression at(int index) {
        return tuple[index - 1];
    }

    @Override
    public Expression evaluate() {
        Expression[] result = new Expression[tuple.length];
        
        for (int i = 0; i < tuple.length; i++) {
            result[i] = tuple[i].evaluate();
        }
        
        return new NTuple(result);
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }

    @Override
    public List<Expression> getChildren() {
        return Collections.unmodifiableList(Arrays.asList(tuple));
    }
    
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder("(");
        
        for (int i = 0; i < tuple.length; i++) {
            if (i == 0) {
                buf.append(tuple[i]);
            } else {
                buf.append(", ");
                buf.append(tuple[i]);
            }
        }
        
        buf.append(")");
        
        return buf.toString();
    }
    
    @Override
    public String toUnicode() {
        return toString();
    }
}
