/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.set;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.bool.TruthValue;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.rule.Rule;

/**
 * The empty set.
 * 
 * @author Elemér Furka
 */
public class EmptySet extends AbstractExpression implements Set {

    @Override
    public Set evaluate() {
        return this;
    }

    @Override
    public TruthValue contains(Expression element) {
        return TruthValue.valueOf(false);
    }

    @Override
    public boolean subsetOf(Set set) {
        return true;
    }

    @Override
    public Set union(Set set) {
        return set;
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }
    
    @Override
    public String toString() {
        return "{}";
    }
    
    @Override
    public String toUnicode() {
        return "\u2205";
    }
    
    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (this == object) return true;
        if (object instanceof Set) {
            if (object instanceof EmptySet) {
                return true;
            } else if (object instanceof FiniteSet) {
                return ((FiniteSet) object).cardinality().isZero();
            }
            
            throw new IllegalStateException("Missing implementation: " + object);
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public Set domain() {
        return this;
    }

    @Override
    public Set codomain() {
        return this;
    }

    @Override
    public Cardinality cardinality() {
        return new Cardinality(Naturals.zero());
    }
}
