/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.set;

import jmathexpr.relation.BinaryRelation;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.relation.RelationSymbol;
import jmathexpr.set.op.CartesianProduct;

/**
 * A binary relation that tests if the left argument is element (member) of the
 * set represented by the right argument.
 * 
 * @author Elemér Furka
 */
public class ElementOf extends BinaryRelation {
    
    public ElementOf(Expression lhs, Expression rhs) {
        super(lhs, rhs, RelationSymbol.ElementOf);
    }
    
    @Override
    public Expression evaluate() {
        Expression l = lhs.evaluate();
        Expression r = rhs.evaluate();
        
        if (r instanceof Set) {
            return ((Set) r).contains(l);
        } else {
            return new ElementOf(l, r);
        }
    }

    @Override
    public boolean isConstant() {
        return lhs.isConstant() && rhs.isConstant();
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Comparison;
    }

    @Override
    protected ElementOf create(Expression lhs, Expression rhs) {
        return new ElementOf(lhs, rhs);
    }

    @Override
    public boolean isSymmetric() {
        return false;
    }

    @Override
    public Set domain() {
        return new CartesianProduct(lhs.domain(), rhs.domain());
    }
}
