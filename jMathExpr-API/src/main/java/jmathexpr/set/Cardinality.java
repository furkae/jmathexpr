/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.set;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.natural.Naturals;

/**
 * Cardinality (number of elements) of a set.
 * 
 * @author Elemér Furka
 */
public class Cardinality extends AbstractExpression {
    
    private final Expression value;
    
    /**
     * Creates a new Cardinality instance representing the specified value.
     * 
     * @param value a valid cardinality value
     */
    public Cardinality(Expression value) {
        this.value = value;
    }
    
    /**
     * Returns the nth aleph number.
     * 
     * @param n a natural number
     * @return the nth aleph number
     */
    public static Cardinality aleph(NaturalNumber n) {
        return new Cardinality(new Aleph(n));
    }
    
    /**
     * Convenience method for aleph(NaturalNumber).
     * 
     * @param n a non-negative integer (Java long)
     * @return the nth aleph number
     */
    public static Cardinality aleph(long n) {
        return aleph(Naturals.getInstance().create(n));
    }
    
    /**
     * Tests whether this is a cardinality of a finite set.
     * 
     * @return true if this is a cardinality of a finite set
     */
    public boolean isFinite() {
        return value instanceof NaturalNumber;
    }
    
    /**
     * Convenience method to check if set's cardinality is zero.
     * 
     * @return true if this is the cardinality of the empty set
     */
    public boolean isZero() {
        return isFinite() && ((NaturalNumber) value).isZero();
    }

    @Override
    public Expression evaluate() {
        return value;
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }
    
    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public String toUnicode() {
        return value.toUnicode();
    }

    public static class Aleph extends AbstractExpression {
        
        private final NaturalNumber value;
        
        private Aleph(NaturalNumber value) {
            this.value = value;
        }

        @Override
        public Expression evaluate() {
            return this;
        }

        @Override
        public boolean isConstant() {
            return true;
        }

        @Override
        public Precedence getPrecedence() {
            return Precedence.Evaluation;
        }
        
        @Override
        public String toString() {
            return "aleph-" + value;
        }

        @Override
        public String toUnicode() {
            return "\u2135" + value;
        }        
    }
}
