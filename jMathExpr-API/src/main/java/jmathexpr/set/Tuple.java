/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.set;

import jmathexpr.Expression;

/**
 * An n-tuple is an order structure of n elements.
 * 
 * @author Elemér Furka
 */
public interface Tuple extends Expression {
    
    /**
     * Gets the number of elements of this tuple.
     */
    int length();
    
    /**
     * Gets the expression at the specified (1-based) index.
     * 
     * @param index element index
     */
    Expression at(int index);
}
