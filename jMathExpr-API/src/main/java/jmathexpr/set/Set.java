/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.set;

import jmathexpr.Expression;
import jmathexpr.bool.TruthValue;

/**
 *
 * @author Elemér Furka
 */
public interface Set extends Expression {
    
    @Override
    Set evaluate();
    
    /**
     * Tests whether the given object is element of this set.
     * 
     * @param element an arbitrary expression
     * @return true if the element is contained in the set
     */
    TruthValue contains(Expression element);
    
    /**
     * Tests if this set is a subset of the other set.
     * 
     * @param set an arbitrary set
     * @return true iff this set is contained in the other set or the two sets
     * are equal
     */
    boolean subsetOf(Set set);
    
    /**
     * Union of this set with another one.
     * 
     * @param set another set
     * @return a new set that contains all elements of both sets
     */
    Set union(Set set);
    
    /**
     * Returns the cardinality of this set.
     * 
     * @return the number of elements of this set
     */
    Cardinality cardinality();
}
