/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr;

import jmathexpr.set.Set;
import jmathexpr.util.pattern.ExpressionPattern;

/**
 * A constant in a mathematical expression.
 * 
 * @author Elemér Furka
 */
public class Constant extends Reference {
    
    /**
     * Creates a constant expression pattern called "c".
     */
    public Constant() {
        this("c", null);
    }
    
    /**
     * Creates a constant with the given name over the given domain.
     * 
     * @param constant the name of this constant
     * @param domain the possible values of this constant
     */
    public Constant(String constant, Set domain) {
        super(constant, domain);
    }
    
    /**
     * Creates a constant with the given name and having the specified value.
     * The constant's domain is set to its value's domain automatically.
     * 
     * @param constant the name of this constant
     * @param value the value of this constant
     */
    public Constant(String constant, Expression value) {
        super(constant, value.domain(), value);
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public Expression hit() {
        return value;
    }

    @Override
    public boolean matches(Expression expr) {
        if (expr.isConstant()) {
            setValue(expr);
            
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(ExpressionPattern pattern) {
        return pattern.matches(this);
    }
}
