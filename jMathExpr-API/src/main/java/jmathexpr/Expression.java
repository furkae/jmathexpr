/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr;

import java.util.List;
import jmathexpr.set.Set;
import jmathexpr.util.logging.ExpressionInfo;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.rule.Rule;

/**
 * An arbitrary mathematical expression.
 * 
 * @author Elemér Furka
 */
public interface Expression {
    
    /**
     * Tries to evaluate the expression.
     * 
     * @return the evaluated expression 
     */
    Expression evaluate();
    
    /**
     * Tests whether this expression is constant, e.g. it does not contain any variable.
     * 
     * @return true if this expression does not depend on any variable
     */
    boolean isConstant();
    
    /**
     * Tests whether this expression contains a subexpression that matches
     * the specified expression pattern.
     * 
     * @param pattern the expression pattern to match against
     * @return true if given pattern matches this expression
     */
    boolean contains(ExpressionPattern pattern);
    
    /**
     * Tests whether the specified rule can be applied to this expression or to 
     * one of its subexpressions. In the latter case implementations must create a
     * new expression instance using the transformed subexpressions and register
     * it using Rule#register(Expression).
     * 
     * @param rule a Rule instance
     * @return true if this expression or one of its subexpressions matches the
     * rule pattern
     */
    boolean isApplicable(Rule rule);
    
    /**
     * Returns this expression's evaluation precedence. Precedence instances
     * can be compared using Precedence#lt(Precedence) or Precedence#le(Precedence)
     * calls.
     * 
     * @return a Precedence instance
     */
    Precedence getPrecedence();
    
    /**
     * Returns the domain of this expression. The definition of the domain is
     * expression type specific. Example: domain of a variable is the set of its
     * possible values.
     * 
     * @return a set as this expression's domain
     */
    Set domain();
    
    /**
     * The codomain of this expression. The codomain of an expression is the set
     * of possible values while the expression is evaluated over its domain.
     * 
     * @return the expression's codomain as a set
     */
    Set codomain();
    
    /**
     * Returns the subexpressions of this expression (if any).
     * 
     * @param <T> any concrete Expression subclass or sub-interface
     * @return all subexpressions of this expression or an empty list
     */
    <T extends Expression> List<T> getChildren();
    
    /**
     * Creates the string representation of this expression using unicode
     * mathematical symbols if appropriate.
     * 
     * @return the string representation using unicode symbols
     * @see #toString()
     */
    String toUnicode();
    
    /**
     * Returns the string representation in ASCIIMath notation. Surrounding back-quote delimiters
     * (``) must not be present.
     * 
     * @return this expression in ASCIIMath notation without back-quotes
     * @see http://www1.chapman.edu/~jipsen/mathml/asciimath.html
     */
    String toAsciiMath();
    
    /**
     * Dumps its content into the input list. This method must only be used for
     * logging purposes.
     * 
     * @param dump a list of dump information
     * @param info the ExpressionInfo instance this expression must use
     */
    void dump(List<ExpressionInfo> dump, ExpressionInfo info);
}
