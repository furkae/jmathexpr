/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr;

import java.util.Collections;
import java.util.List;
import jmathexpr.set.Set;
import jmathexpr.util.logging.ExpressionInfo;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.rule.Rule;

/**
 * This class provides trivial implementation of all methods in the interface
 * Expression.
 * 
 * @author Elemér Furka
 */
public abstract class AbstractExpression implements Expression {

    @Override
    public Expression evaluate() {
        return this;
    }

    @Override
    public boolean isConstant() {
        for (Expression e : getChildren()) {
            if (!e.isConstant()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean contains(ExpressionPattern pattern) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isApplicable(Rule rule) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Precedence getPrecedence() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set domain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set codomain() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T extends Expression> List<T> getChildren() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public String toUnicode() {
        return toString();
    }

    @Override
    public String toAsciiMath() {
        return toString();
    }
    
    @Override
    public void dump(List<ExpressionInfo> dump, ExpressionInfo info) {
        dump.add(info);
        
        List<Expression> children = getChildren();
        ExpressionInfo childInfo = null;
        
        for (Expression c : children) {
            if (childInfo == null) {
                childInfo = info.firstChild(c);
            } else {
                childInfo = childInfo.nextSibling(c);
            }
            
            c.dump(dump, childInfo);
        }
    }
}
