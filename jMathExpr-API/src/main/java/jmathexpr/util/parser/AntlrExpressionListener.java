/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.parser;

import java.util.Deque;
import java.util.LinkedList;

import jmathexpr.Expression;
import jmathexpr.Variable;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.equation.Equation;
import jmathexpr.arithmetic.func.Abs;
import jmathexpr.arithmetic.func.Exp;
import jmathexpr.arithmetic.func.Ln;
import jmathexpr.arithmetic.func.Sqrt;
import jmathexpr.arithmetic.integer.IntegerNumber;
import jmathexpr.arithmetic.integer.Integers;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.op.Division;
import jmathexpr.arithmetic.op.Exponentiation;
import jmathexpr.arithmetic.op.Multiplication;
import jmathexpr.arithmetic.op.Negation;
import jmathexpr.arithmetic.op.Sum;
import jmathexpr.arithmetic.rational.Rationals;
import jmathexpr.arithmetic.real.Reals;
import jmathexpr.relation.Equality;
import jmathexpr.set.ElementOf;
import jmathexpr.set.Set;
import jmathexpr.util.context.ExpressionContext;
import jmathexpr.util.context.Statement;
import jmathexpr.util.context.Statement.Command;


/**
 *
 * @author Elemér Furka
 */
public class AntlrExpressionListener extends ExpressionsBaseListener {
    
    private final Deque<Expression> stack = new LinkedList();
    
    private Variable x;
    
    private Statement statement;
    
    /**
     * Returns the parsed expression.
     * 
     * @return 
     */
    Expression getExpression() {
        return stack.pop();
    }
    
    /**
     * Returns the result of the parsing.
     * 
     * @return the parsed Statement instance
     */
    Statement getStatement() {
        if (statement != null) {
            return statement;
        } else {
            return new Statement(Command.Expression, getExpression());
        }
    }
    
    @Override
    public void exitLet(ExpressionsParser.LetContext ctx) {
        Set domain = (Set) stack.pop();
        Variable tmp = (Variable) stack.pop();
        
        Variable x = new Variable(tmp.name(), domain);
        Expression contains = new ElementOf(x, domain);
        
        statement = new Statement(Command.Let, contains);
        
        ExpressionContext.getInstance().addVariable(x);
        stack.push(contains);
    }

    @Override
    public void exitSolve(ExpressionsParser.SolveContext ctx) {
        Equation equation = (Equation) stack.pop();
        
        statement = new Statement(Command.Solve, equation);
        
        stack.push(equation);
    }
    
    @Override
    public void exitAdditive(ExpressionsParser.AdditiveContext ctx) {
        Expression rhs = stack.pop();
        Expression lhs = stack.pop();
        
        if (ctx.operator.getType() == ExpressionsParser.ADD) {
            stack.push(Sum.add(lhs, rhs));
        } else if (ctx.operator.getType() == ExpressionsParser.SUB) {
            stack.push(Sum.subtract(lhs, rhs));
        } else {
            throw new IllegalStateException("Unexpected operation: " + ctx.operator);
        }
    }
    
    @Override
    public void exitMultiplication(ExpressionsParser.MultiplicationContext ctx) {
        Expression rhs = stack.pop();
        Expression lhs = stack.pop();
        
        stack.push(new Multiplication(lhs, rhs));
    }
    
    @Override
    public void exitSimpleMultiplication(ExpressionsParser.SimpleMultiplicationContext ctx) {
        Expression rhs = stack.pop();
        Expression lhs = stack.pop();
        
        stack.push(new Multiplication(lhs, rhs));
    }
    
    @Override
    public void exitDivision(ExpressionsParser.DivisionContext ctx) {
        Expression rhs = stack.pop();
        Expression lhs = stack.pop();
        
        if (lhs instanceof IntegerNumber && rhs instanceof NaturalNumber) {
            stack.push(new Division(lhs, rhs).evaluate());
        } else {
            stack.push(new Division(lhs, rhs));
        }
    }
    
    @Override
    public void exitExponentiation(ExpressionsParser.ExponentiationContext ctx) {
        Expression rhs = stack.pop();
        Expression lhs = stack.pop();
        
        stack.push(new Exponentiation(lhs, rhs));
    }
    
    @Override
    public void exitNegation(ExpressionsParser.NegationContext ctx) {
        Expression arg = stack.pop();
        
        if (arg instanceof ANumber) {
            stack.push(((ANumber) arg).negate());
        } else {
            stack.push(new Negation(arg));
        }
    }
    
    @Override
    public void exitEquation(ExpressionsParser.EquationContext ctx) {
        Expression rhs = stack.pop();
        Expression lhs = stack.pop();
        Equality equation = new Equality(lhs, rhs);
        
        stack.push(Equation.convert(equation, x));
    }
    
    @Override
    public void exitFunction(ExpressionsParser.FunctionContext ctx) {
        String function = ctx.FUNC().getText();
        Expression arg = stack.pop();
        
        switch (function) {
            case "exp":
                stack.push(new Exp(arg));
                break;
            
            case "ln":
                stack.push(new Ln(arg));
                break;
            
            case "sqrt":
                stack.push(new Sqrt(arg));
                break;
            
            case "abs":
                stack.push(new Abs(arg));
                break;
            
            default:
                throw new IllegalStateException("Unexpected function name: " + function);
        }
    }
    
    @Override
    public void exitNumber(ExpressionsParser.NumberContext ctx) {
        stack.push(Naturals.getInstance().create(ctx.getText()));
    }
    
    @Override
    public void exitVariable(ExpressionsParser.VariableContext ctx) {
        x = ExpressionContext.getInstance().getVariable(ctx.getText());
        
        if (x == null) {
            x = Numbers.variable(ctx.getText());
            ExpressionContext.getInstance().addVariable(x);
        }
        
        stack.push(x);
    }
    
    @Override
    public void enterSet(ExpressionsParser.SetContext ctx) {
        String set = ctx.getText();
        
        switch (set) {
            case "N":
                stack.push(Naturals.getInstance());
                break;
            
            case "Z":
                stack.push(Integers.getInstance());
                break;
            
            case "Q":
                stack.push(Rationals.getInstance());
                break;
            
            case "R":
                stack.push(Reals.getInstance());
                break;
            
            default:
                throw new IllegalStateException("Unexpected function name: " + set);
        }
    }
    
    @Override
    public void exitAbs(ExpressionsParser.AbsContext ctx) {
        Expression arg = stack.pop();
        
        stack.push(new Abs(arg));
    }
//    
//    @Override
//    public void enterEveryRule(ParserRuleContext ctx) {
//        System.out.printf("in:  %s : %s%n", ctx.getText(), ctx.toStringTree());
//    }
//
//    @Override
//    public void exitEveryRule(ParserRuleContext ctx) {
//        System.out.printf("out: %s : %s%n", ctx.getText(), ctx.toStringTree());
//    }
}
