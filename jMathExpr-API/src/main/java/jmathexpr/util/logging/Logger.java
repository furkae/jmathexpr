/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.logging;

import java.util.ArrayList;
import java.util.List;
import jmathexpr.Expression;

/**
 * Base class of all logging activities.
 * 
 * @author Elemér Furka
 */
public class Logger {
    
    /**
     * Dumps all relevant information (its structure) about an expression to the
     * stdout.
     * 
     * @param expr an arbitrary expression
     */
    public static void dump(Expression expr) {
        List<ExpressionInfo> dump = new ArrayList();
        
        expr.dump(dump, ExpressionInfo.root(expr));
        
        for (ExpressionInfo i : dump) {
            System.out.println(getIndent(i.level()) + i);
        }
    }
    
    private static String getIndent(int length) {
        StringBuilder buf = new StringBuilder();
        
        for (int i = 0; i < length; i++) {
            buf.append("..");
        }
        
        return buf.toString();
    }
    
    /**
     * Prints out the caller of the current method.
     */
    public static void caller() {
        System.out.println(Thread.currentThread().getStackTrace()[2]);
    }
}
