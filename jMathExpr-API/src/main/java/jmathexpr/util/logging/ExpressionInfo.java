/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.logging;

import java.util.ArrayList;
import java.util.List;
import jmathexpr.Expression;

/**
 * Container class for all expression related information that is produced while
 * dumping an expression.
 * 
 * @author Elemér Furka
 */
public class ExpressionInfo {
    
    private final Id id;
    
    private final Expression expression;

    private ExpressionInfo(Id id, Expression expression) {
        this.id = id;
        this.expression = expression;
    }
    
    /**
     * Creates the root ExpressionInfo instance that has the id 0.
     * 
     * @param expr the root expression
     * @return the root ExpressionInfo object
     */
    public static ExpressionInfo root(Expression expr) {
        return new ExpressionInfo(Id.root(), expr);
    }
    
    /**
     * Creates a new ExpressionInfo instance that will be the (first) child of
     * this ExpressionInfo object.
     * 
     * @param expr the appropriate subexpression
     * @return a new ExpressionInfo instance that represents the first child of
     * its parent
     */
    public ExpressionInfo firstChild(Expression expr) {
        return new ExpressionInfo(id.firstChild(), expr);
    }
    
    /**
     * Creates a new ExpressionInfo instance that will be the next sibling after
     * this instance.
     * 
     * @param expr the appropriate subexpression
     * @return a new ExpressionInfo instance that represents the next sibling in
     * the subexpression list
     */
    public ExpressionInfo nextSibling(Expression expr) {
        return new ExpressionInfo(id.nextSibling(), expr);
    }
    
    /**
     * Returns the actual level of the expression belonging to this ExpressionInfo
     * instance. The root instance has level 0.
     * 
     * @return the zero based level
     */
    public int level() {
        return id.level();
    }
    
    @Override
    public String toString() {
//        return String.format("%s: %s (%s)", id, expression, expression.getClass().getSimpleName());
        return String.format("%s (%s)", expression, expression.getClass().getSimpleName());
    }
    
    private static class Id {
        
        private final List<Integer> items = new ArrayList();
        
        private static Id root() {
            Id id = new Id();
            
            id.items.add(0);
            
            return id;
        }
                
        private Id firstChild() {
            Id id = new Id();
            
            id.items.addAll(items);
            id.items.add(1);
            
            return id;
        }
        
        private Id nextSibling() {
            Id id = new Id();
            int lastIndex = items.size() - 1;
            
            id.items.addAll(items);
            id.items.set(lastIndex, items.get(lastIndex) + 1);
            
            return id;            
        }
        
        private int level() {
            return items.size() - 1;
        }
        
        @Override
        public String toString() {
            StringBuilder buf = null;
            
            for (Integer i : items) {
                if (buf == null) {
                    buf = new StringBuilder();
                } else {
                    buf.append(".");
                }
                
                buf.append(i);
            }
            
            return buf.toString();
        }
    }
}
