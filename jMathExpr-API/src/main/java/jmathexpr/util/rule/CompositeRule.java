/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.rule;

import jmathexpr.Expression;

/**
 * Abstract base class of composite rules (rules that contain more than one rule
 * variants as inner class).
 * 
 * @author Elemér Furka
 */
public abstract class CompositeRule implements Rule {
    
    protected final boolean recursive;

    protected SubRule rule;
    
    protected Expression target;
    
    protected CompositeRule(boolean recursive) {
        this.recursive = recursive;
    }

    @Override
    public final Expression apply() {
        return rule != null ? rule.apply() : target;
    }

    @Override
    public void register(Expression expr) {
        target = expr;
        rule = null;
    }
    
    @Override
    public boolean isRecursive() {
        return recursive;
    }

    @Override
    public Expression subapply() {
        return apply();
    }

    @Override
    public void reset() {
        rule = null;
        target = null;
    }
    
    @Override
    public String toString() {
        if (rule == null && target == null) {
            return String.format("%s", getClass().getSimpleName());
        } else {
            return rule == null ? String.format("%s[%s]", getClass().getSimpleName(), target)
                    : String.format("%s[%s, %s]", getClass().getSimpleName(), target, rule);
        }
    }
}
