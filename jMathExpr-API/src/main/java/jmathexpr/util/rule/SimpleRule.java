/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.rule;

import jmathexpr.Expression;

/**
 * Abstract base class of simple rules.
 * 
 * @author Elemér Furka
 */
public abstract class SimpleRule implements Rule {
    
    protected Expression target;

    @Override
    public void register(Expression expr) {
        target = expr;
    }
    
    @Override
    public boolean isRecursive() {
        return false;
    }

    @Override
    public Expression subapply() {
        return target;
    }

    @Override
    public void reset() {
        target = null;
    }
    
    @Override
    public String toString() {
        return target == null ? String.format("%s", getClass().getSimpleName())
                : String.format("%s[%s]", getClass().getSimpleName(), target);
    }
}
