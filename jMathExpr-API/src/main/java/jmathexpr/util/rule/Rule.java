/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.rule;

import jmathexpr.Expression;

/**
 * Represents a mathematical property, law or rule. Example: the distributive law.
 * The general client side usage of a class implementing this interface looks as
 * follows:
 * <code>
 * Expression expr = ...;
 * Rule = new ...;
 * if (expr.isApplicable(rule)) expr = rule.apply();
 * </code>
 * 
 * @author Elemér Furka
 */
public interface Rule {
    
    /**
     * Tests whether the given expression can match one of the patterns of this rule.
     * 
     * @param expr
     * @return if the rule can be applied on this expression
     */
    boolean matches(Expression expr);
    
    /**
     * Applies the rule after one of the pattern has matched the expression.
     * 
     * @return a new expression instance that is created by the transformation of
     * the original expression
     */
    Expression apply();
    
    /**
     * Performs this rule's action on the actual subexpression (child expression).
     * Implementation note: this method must only be called from Expression#isApplicable(Rule)
     * if the rule is recursive.
     * 
     * @return the transformed subexpression
     */
    Expression subapply();
    
    /**
     * Informs this rule that the next call to apply() should return the registered
     * expression. This method must only be called inside Expression.isApplicable(Rule).
     * 
     * @param expr a new expression that already has at least of of its subexpressions
     * substituted by this rule
     */
    void register(Expression expr);
    
    /**
     * Returns if this rule's action must be applied on each matching subexpression.
     * This method should be used in Expression#isApplicable(Rule) implementations
     * to decide if the rule must be applied to match subexpressions too.
     * 
     * @return false if the rule must not change subexpressions (whether or not
     * the subexpression matches the rule's pattern)
     */
    boolean isRecursive();
    
    /**
     * Resets the internal state of this rule so that it can be reused again.
     */
    void reset();
}
