/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.rule;

import jmathexpr.Expression;

/**
 * Abstract base class of subrules (rule classes existing as composite rule
 * inner classes).
 * 
 * @author Elemér Furka
 */
public abstract class SubRule implements Rule {

    @Override
    public void register(Expression expr) {
        throw new UnsupportedOperationException("This method must be overridden.");
    }
    
    @Override
    public boolean isRecursive() {
        throw new UnsupportedOperationException("This method must be overridden.");
    }

    @Override
    public Expression subapply() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
