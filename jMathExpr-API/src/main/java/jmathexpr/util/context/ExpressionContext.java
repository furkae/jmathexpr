/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jmathexpr.Expression;

import jmathexpr.Variable;
import jmathexpr.util.context.Statement.Command;

/**
 * The expression context contains statements, expressions, variables and history
 * on a thread local basis.
 * 
 * @author Elemér Furka
 */
public class ExpressionContext {
    
    private final Map<String, Variable> variables = new HashMap();
    
    // The (dummy) root item.
    private final Item root = new Item();
    
    // The actual (parent) item. New items are added as children of this instance.
    private Item actual = root;
    
    private static final ThreadLocal<ExpressionContext> context
            = new ThreadLocal<ExpressionContext>() {
                @Override
                protected ExpressionContext initialValue() {
                    return new ExpressionContext();
                }
            };
    
    /**
     * Returns this thread's expression context.
     * 
     * @return an ExpressionContext assigned to the current thread
     */
    public static ExpressionContext getInstance() {
        return context.get();
    }
    
    /**
     * Adds a new variable to this context.
     * 
     * @param var an arbitrary variable
     */
    public void addVariable(Variable var) {
        variables.put(var.name(), var);
    }
    
    /**
     * Retrieves a variable from this context.
     * 
     * @param var the name of the variable
     * @return the variable having the given name or null if it does not exists
     * in this context
     */
    public Variable getVariable(String var) {
        return variables.get(var);
    }
    
    public void addStatement(Statement statement) {
        actual.createChild(statement);
    }
    
    /**
     * Adds an expression to this context. The statement used has Command.Expression.
     * 
     * @param expression an arbitrary expression
     */
    public void addExpression(Expression expression) {
        addStatement(new Statement(Command.Expression, expression));
    }
    
    public List<Item> getFirstLevelItems() {
        return Collections.unmodifiableList(root.children);
    }
    
    /**
     * An item corresponds to a line on a whiteboard. It can be a statement or an
     * expression. Items are stored hierarchically: an item can have child items.
     */
    public class Item {
        
        private final Statement statement;
        
        // id > 0
        private final Integer id;
        
        private final Item parent;
        
        private List<Item> children;
        
        // Creates the (dummy) root item
        private Item() {
            statement = null;
            id = null;
            parent = null;
            
            children = new ArrayList();
        }
        
        private Item(Statement statement, Item parent) {
            this.statement = statement;
            this.parent = parent;
            
            if (parent.children == null) {
                parent.children = new ArrayList();
            }
            
            id = parent.children.size() + 1;
        }
        
        private void createChild(Statement statement) {
            Item item = new Item(statement, this);
            
            System.out.printf("%s : %s%n", this, children);
            children.add(item);
        }
        
        /**
         * Returns the statement associated to this item.
         * 
         * @return the appropriate statement
         */
        public Statement getStatement() {
            return statement;
        }
        
        /**
         * Returns this item's id.
         * 
         * @return the parent item's id (if any) combined with this item's id.
         */
        public String getId() {
            if (parent == root) {
                return Integer.toString(id);
            } else {
                return parent.getId() + "." + id;
            }
        }
        
        @Override
        public String toString() {
            if (this == root) {
                return "<root>";
            } else {
                return String.format("%s: %s", getId(), statement);
            }
        }
    }
}
