/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.context;

import jmathexpr.Expression;

/**
 * This class represents a statement, that is an expression and a command that
 * must be executed using the expression.
 * 
 * @author Elemér Furka
 */
public class Statement {
    
    private final Command command;
    
    private final Expression expression;
    
    public Statement(Command command, Expression expression) {
        this.command = command;
        this.expression = expression;
    }

    /**
     * Returns the command.
     * 
     * @return a Command instance
     */
    public Command getCommand() {
        return command;
    }

    /**
     * Returns the mathematical expression.
     * 
     * @return an Expression instance
     */
    public Expression getExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return command + " " + expression;
    }
    
    public enum Command {
        
        Expression("expression"), // Dummy command. Adds a new expression to the context.
        Let("let"),               // Defines a new variable or constant.
        Solve("solve");           // Solves an equation.
        
        private final String command;
        
        private Command(String command) {
            this.command = command;
        }
        
        @Override
        public String toString() {
            return command;
        }
    }
}
