/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.pattern;

import jmathexpr.Expression;

/**
 * This class represents the negation pattern: it matches an expression if the
 * expression does not match this pattern's subpattern.
 * 
 * @author Elemér Furka
 */
public class NotPattern extends AbstractPattern {
    
    private final ExpressionPattern subpattern;
    
    public NotPattern(ExpressionPattern subpattern) {
        this.subpattern = subpattern;
    }
    
    @Override
    public boolean matches(Expression expr) {
        if (!subpattern.matches(expr)) {
            value = expr;
            
            return true;
        }
        
        return false;
    }

    @Override
    public String toString() {
        return value == null ? String.format("not(%s)", subpattern)
                : String.format("not(%s)[%s]", subpattern, value);
    }
}
