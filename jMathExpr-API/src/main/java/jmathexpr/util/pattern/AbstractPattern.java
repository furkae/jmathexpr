/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.pattern;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;

/**
 * Base class of most expression pattern implementations.
 * 
 * @author Elemér Furka
 */
public abstract class AbstractPattern extends AbstractExpression implements TerminationPattern {
    
    protected Expression value;

    @Override
    public Expression hit() {
        return value;
    }

    @Override
    public Expression evaluate() {
        return value;
    }

    @Override
    public boolean isConstant() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }

    @Override
    public String toUnicode() {
        return toString();
    }
}
