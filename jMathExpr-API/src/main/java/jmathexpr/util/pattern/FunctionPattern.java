/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.pattern;

import jmathexpr.Expression;

/**
 * This class represents a function pattern of a specific variable.
 * 
 * @author Elemér Furka
 */
public class FunctionPattern extends AbstractPattern {
    
    private final ExpressionPattern arg;
    
    /**
     * Creates a new pattern using the specified argument pattern.
     * 
     * @param arg the argument pattern
     */
    public FunctionPattern(ExpressionPattern arg) {
        this.arg = arg;
    }
    
    @Override
    public String toString() {
        return String.format("f(%s)", arg);
    }

    @Override
    public boolean matches(Expression expr) {
        if (expr.contains(arg)) {
            value = expr;
            
            return true;
        } else {
            return false;
        }
    }
}
