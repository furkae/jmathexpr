/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.pattern;

import jmathexpr.Expression;

/**
 * A terminal pattern is an expression pattern that represents a terminal
 * expression (e.g. a number, a variable, a constant).
 * 
 * @author Elemér Furka
 */
public interface TerminationPattern extends ExpressionPattern {
    
    /**
     * Returns the hit of the last successful match.
     * 
     * @return the expression that has matched this pattern
     */
    Expression hit();
}
