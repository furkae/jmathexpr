/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.pattern;

import jmathexpr.Expression;

/**
 * This interface represents an expression pattern that can be used
 * to find matching expressions. A pattern matches an expression if the expression
 * is built from the parts defined by the pattern.
 * 
 * Each implementation must also implement Expression so that they can be used
 * as operation arguments.
 * 
 * @author Elemér Furka
 */
public interface ExpressionPattern extends Expression {
    
    /**
     * Tests whether this pattern matches the given expression.
     * Constants and variables
     * in the pattern must be substituted if this method returns true.

     * @param expr an arbitrary expression
     * @return true if this pattern matches the given expression
     */
    boolean matches(Expression expr);
}
