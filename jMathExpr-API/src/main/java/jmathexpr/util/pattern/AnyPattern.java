/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.util.pattern;

import jmathexpr.Expression;

/**
 * This class represents an arbitrary expression in an expression pattern that
 * matches any expression.
 * 
 * @author Elemér Furka
 */
public class AnyPattern extends AbstractPattern {
    
    @Override
    public boolean matches(Expression expr) {
        value = expr;
        
        return true;
    }

    @Override
    public String toString() {
        return "<any>";
    }
}
