/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.relation.order;

import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.relation.BinaryRelation;
import jmathexpr.relation.RelationSymbol;

/**
 *
 * @author Elemér Furka
 */
public abstract class TotalOrder extends BinaryRelation {

    protected TotalOrder(Expression lhs, Expression rhs, RelationSymbol symbol) {
        super(lhs, rhs, symbol);
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Comparison;
    }
}
