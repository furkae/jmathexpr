/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.relation;

import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.bool.TruthValue;
import jmathexpr.arithmetic.ANumber;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.set.Set;

/**
 * The binary relation equality (=).
 * 
 * @author Elemér Furka
 */
public class Equality extends BinaryRelation {
    
    public Equality(Expression lhs, Expression rhs) {
        super(lhs, rhs, RelationSymbol.Equality);
    }
    
    @Override
    public Expression evaluate() {
        Expression l = lhs.evaluate();
        Expression r = rhs.evaluate();
        
        if (l.getClass().equals(r.getClass())) {
            return TruthValue.valueOf(l.equals(r));
        } else if (l instanceof ANumber && r instanceof ANumber) {
            return TruthValue.valueOf(Numbers.equal((ANumber) l, (ANumber) r));
        } else {
            return new Equality(l, r);
        }
    }

    @Override
    public boolean isConstant() {
        return lhs.isConstant() && rhs.isConstant();
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Equality;
    }

    @Override
    protected BinaryRelation create(Expression lhs, Expression rhs) {
        return new Equality(lhs, rhs);
    }

    @Override
    public boolean isSymmetric() {
        return true;
    }

    @Override
    public Set domain() {
        Set d = lhs.domain();
        
        if (d.equals(rhs.domain())) {
            return d;
        } else {
            throw new IllegalStateException(String.format(
                    "Different argument domains: %s : %s", d, rhs.domain()));
        }
    }
}
