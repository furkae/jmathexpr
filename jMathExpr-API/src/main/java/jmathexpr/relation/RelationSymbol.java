/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.relation;

/**
 *
 * @author Elemér Furka
 */
public enum RelationSymbol {
    Equality("=", "=", "="), ElementOf("in", "\u2208", "in"),
    LE("<=", "\u2264", "<="), LT("<", "<", "<"), GE(">=", "\u2265", ">="), GT(">", ">", ">");
    
    private final String sign;
    
    private final String unicodeSign;
    
    private final String asciiMathSign;
    
    private RelationSymbol(String sign, String unicodeSign, String asciiMathSign) {
        this.sign = sign;
        this.unicodeSign = unicodeSign;
        this.asciiMathSign = asciiMathSign;
    }
    
    @Override
    public String toString() {
        return sign;
    }
    
    public String toUnicode() {
        return unicodeSign;
    }
    
    public String toAsciiMath() {
        return asciiMathSign;
    }
}
