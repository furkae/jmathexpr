/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.bool;

import java.util.Collections;
import java.util.List;
import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.set.Set;
import jmathexpr.util.logging.ExpressionInfo;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.rule.Rule;

/**
 * A boolean value (true or false).
 * 
 * @author Elemér Furka
 */
public enum TruthValue implements Expression {
    
    False(false), True(true);
    
    private final boolean value;
    
    private TruthValue(boolean value) {
        this.value = value;
    }
    
    @Override
    public TruthValue evaluate() {
        return this;
    }
    
    public TruthValue and(TruthValue other) {
        return TruthValue.valueOf(value && other.value);
    }
    
    public TruthValue or(TruthValue other) {
        return TruthValue.valueOf(value || other.value);
    }
    
    public TruthValue not() {
        return TruthValue.valueOf(!value);
    }
    
    public static TruthValue valueOf(boolean value) {
        return value ? True : False;
    }
    
    @Override
    public String toString() {
        return Boolean.toString(value);
    }
    
    @Override
    public String toUnicode() {
        return Boolean.toString(value);
    }

    @Override
    public String toAsciiMath() {
        return toString();
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public boolean isApplicable(Rule rule) {
        return rule.matches(this);
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }

    @Override
    public boolean contains(ExpressionPattern pattern) {
        throw new IllegalStateException("Method must be implemented.");
    }
    
    /**
     * Transforms this instance into a Java boolean value.
     * 
     * @return false for False or true for True
     */
    public boolean toBoolean() {
        return value;
    }

    @Override
    public Set domain() {
        return Booleans.getInstance();
    }

    @Override
    public Set codomain() {
        return Booleans.getInstance();
    }

    @Override
    public List<Expression> getChildren() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public void dump(List<ExpressionInfo> dump, ExpressionInfo info) {
        dump.add(info);
    }
}
