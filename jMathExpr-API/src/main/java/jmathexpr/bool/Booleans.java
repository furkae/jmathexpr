/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.bool;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.set.Cardinality;
import jmathexpr.set.Set;

/**
 * The set of truth values (true and false). It's a singleton.
 * 
 * @author Elemér Furka
 */
public class Booleans extends AbstractExpression implements Set {
    
    private static final Booleans INSTANCE = new Booleans();
    
    /**
     * Returns the singleton instance.
     * 
     * @return the singleton instance
     */
    public static Booleans getInstance() {
        return INSTANCE;
    }
    
    private Booleans() { }
    
    @Override
    public Booleans evaluate() {
        return this;
    }

    @Override
    public TruthValue contains(Expression element) {
        return TruthValue.valueOf(element instanceof TruthValue);
    }

    @Override
    public boolean subsetOf(Set set) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set union(Set set) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }

    @Override
    public Set domain() {
        return this;
    }

    @Override
    public Set codomain() {
        return this;
    }
    
    @Override
    public String toString() {
        return "B";
    }
    
    @Override
    public String toUnicode() {
        return toString();
    }

    @Override
    public Cardinality cardinality() {
        return new Cardinality(Naturals.getInstance().create(2));
    }
}
