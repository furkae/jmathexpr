/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr;

import java.util.Objects;
import jmathexpr.set.Set;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.pattern.TerminationPattern;
import jmathexpr.util.rule.Rule;

/**
 * A reference is an identifier that references a specific expression. Typical
 * references are variables and constants. A reference always has a name (e.g. x)
 * and may have a substitution value (an expression).
 * 
 * @author Elemér Furka
 */
public abstract class Reference extends AbstractExpression implements TerminationPattern {
    
    protected final String name;
    
    protected final Set domain;

    protected Expression value;

    /**
     * Creates a named reference. This reference does not have a specific value.
     */
    protected Reference(String name, Set domain) {
        this.name = name;
        this.domain = domain;
    }
    
    /**
     * Creates a named reference and assigns an (initial) value to it.
     */
    public Reference(String name, Set domain, Expression value) {
        this(name, domain);
        
        this.value = value;
    }
    
    @Override
    public Expression evaluate() {
        return value != null && this != value ? value.evaluate() : this;
    }

    @Override
    public boolean isApplicable(Rule rule) {
        return rule.matches(this);
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.Evaluation;
    }

    @Override
    public Set domain() {
        return domain;
    }

    @Override
    public Set codomain() {
        return domain();
    }
    
    /**
     * Sets the (new) value of this reference instance.
     */
    public void setValue(Expression value) {
        this.value = value;
    }
    
    /**
     * Returns the name of this reference.
     * 
     * @return the reference name as a string 
     */
    public String name() {
        return name;
    }
    
    /**
     * Resets this reference so that it does not reference any value.
     */
    public void reset() {
        value = null;
    }

    @Override
    public boolean contains(ExpressionPattern pattern) {
        return pattern.matches(this);
    }
    
    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (this == object) return true;
        if (getClass().equals(object.getClass())) {
            Reference other = (Reference) object;
            
            return name.equals(other.name);
        }
        
        return false;        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.name);
        return hash;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    @Override
    public String toUnicode() {
        return name;
    }
}
