/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.op;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import jmathexpr.AbstractExpression;
import jmathexpr.Expression;
import jmathexpr.Precedence;
import jmathexpr.util.pattern.ExpressionPattern;
import jmathexpr.util.rule.Rule;

/**
 * Represents a unary operation. Example: negation of numbers.
 * 
 * @author Elemér Furka
 */
public abstract class UnaryOperation extends AbstractExpression implements Operation, ExpressionPattern {
    
    protected final Expression operand;
    
    protected final Sign sign;
    
    protected UnaryOperation(Expression operand, Sign sign) {
        this.operand = operand;
        
        this.sign = sign;
    }

    @Override
    public List<Expression> operands() {
        return Collections.singletonList(operand);
    }

    @Override
    public List<Expression> getChildren() {
        return operands();
    }
    
    protected abstract UnaryOperation create(Expression operand);
    
    @Override
    public boolean equals(Object object) {
        if (object == null) return false;
        if (this == object) return true;
        if (object instanceof UnaryOperation) {
            UnaryOperation other = (UnaryOperation) object;
            
            if (sign != other.sign) return false;
            
            return operand.equals(other.operand);
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.operand);
        hash = 59 * hash + Objects.hashCode(this.sign);
        return hash;
    }
    
    @Override
    public String toString() {
        if (operand.getPrecedence().le(getPrecedence())) {
            return String.format("%s(%s)", sign, operand);
        } else {
            return String.format("%s%s", sign, operand);
        }
    }
    
    @Override
    public String toUnicode() {
        if (operand.getPrecedence().le(getPrecedence())) {
            return String.format("%s(%s)", sign.toUnicode(), operand);
        } else {
            return String.format("%s%s", sign.toUnicode(), operand);
        }
    }

    @Override
    public int getArity() {
        return 1;
    }

    @Override
    public boolean isConstant() {
        return operand.isConstant();
    }
    
    @Override
    public boolean contains(ExpressionPattern pattern) {
        if (getClass().isAssignableFrom(pattern.getClass())) {
            UnaryOperation p = (UnaryOperation) pattern;
            
            if (operand.contains((ExpressionPattern) p.operand)) {
                return true;
            }
        }
        
        return operand.contains(pattern);
    }

    @Override
    public boolean isApplicable(Rule rule) {
        boolean isApplicable = false;
        Expression transformed = this;
        
        if (rule.isRecursive()) {
            if (operand.isApplicable(rule)) {
                isApplicable = true;
                transformed = create(rule.subapply());
            }
        }
        
        if (rule.matches(transformed)) {
            return true;
        } else if (isApplicable) {
            rule.register(transformed);
        }
        
        return isApplicable;
    }

    @Override
    public Precedence getPrecedence() {
        return Precedence.UnaryOperation;
    }

    @Override
    public boolean matches(Expression expr) {
        if (expr.getClass().isAssignableFrom(getClass())) {
            UnaryOperation other = (UnaryOperation) expr;
            
            if (((ExpressionPattern) operand).matches(other.operand)) {
                return true;
            }
        }
        
        return false;
    }
}
