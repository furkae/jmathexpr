/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.op;

/**
 * List of operation signs.
 * 
 * @author Elemér Furka
 */
public enum Sign {
    Addition('+', "+"), Subtraction('-', "-"),
    Multiplication('*', "\u22C5"), Cross('x', "\u00D7"), Division('/', "/"),
    Exponentiation('^', "^"), Difference('\\', "\u2216");
    
    private final char sign;
    
    private final String unicodeSign;
    
    private Sign(char sign, String unicodeSign) {
        this.sign = sign;
        this.unicodeSign = unicodeSign;
    }
    
    @Override
    public String toString() {
        return Character.toString(sign);
    }
    
    public String toUnicode() {
        return unicodeSign;
    }
}
