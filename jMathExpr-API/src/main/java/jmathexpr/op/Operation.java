/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr.op;

import java.util.List;
import jmathexpr.Expression;
import jmathexpr.func.Function;

/**
 * A mathematical operation (e.g. addition).
 * 
 * @author Elemér Furka
 */
public interface Operation extends Function {
    
    /**
     * Returns this operation's operands.
     * 
     * @return a list of operands
     */
    List<Expression> operands();
    
    /**
     * Returns the arity of this operation
     * 
     * @return the number of operands this operation accepts
     */
    int getArity();
}
