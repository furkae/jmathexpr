/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr;

import jmathexpr.arithmetic.Polynomial;
import jmathexpr.set.Set;
import jmathexpr.util.pattern.ExpressionPattern;

/**
 * A variable in a mathematical expression.
 * 
 * @author Elemér Furka
 */
public class Variable extends Reference {
    
    /**
     * Creates a variable expression pattern called "x".
     */
    public Variable() {
        this("x", null);
    }
    
    /**
     * Creates a variable with the given name over the given domain.
     * 
     * @param var the name (identifier) of this variable
     * @param domain the possible values of this variable
     */
    public Variable(String var, Set domain) {
        super(var, domain);
    }
    
    /**
     * Creates a variable with the given name and having the specified (initial) value.
     * The variable's domain is set to its value's domain automatically.
     * 
     * @param var the name of this variable
     * @param value the value of this variable
     */
    public Variable(String var, Expression value) {
        super(var, value.domain(), value);
    }
    
    @Override
    public boolean isConstant() {
        return false;
    }

    @Override
    public boolean matches(Expression expr) {
        if (expr instanceof Variable) {
            setValue(expr);
            
            return true;
        } else if (expr instanceof Polynomial) {
            return ((Polynomial) expr).matches(this);
        } else {
            return false;
        }
    }

    @Override
    public Expression hit() {
        return value;
    }

    @Override
    public boolean contains(ExpressionPattern pattern) {
        return pattern.matches(this);
    }
}
