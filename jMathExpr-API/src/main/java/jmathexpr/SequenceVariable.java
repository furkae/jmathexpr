/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmathexpr;

import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.set.Set;

/**
 * A sequence variable in a mathematical expression. Examples: a1, a2, ..., an
 * 
 * @author Elemér Furka
 */
public class SequenceVariable extends Variable {
    
    private final Expression index;
    
    /**
     * Creates a variable expression pattern called "an".
     */
    public SequenceVariable() {
        this("a", new Constant("n", null), null);
    }
    
    /**
     * Creates a variable with the given name and index over the given domain.
     * 
     * @param var the name (identifier) of this variable
     * @param index the variable index
     * @param domain the possible values of this variable
     */
    public SequenceVariable(String var, Expression index, Set domain) {
        super(var, domain);
        
        this.index = index;
    }
    
    /**
     * Creates a variable with the given name and having the specified (initial) value.
     * The variable's domain is set to its value's domain automatically.
     * 
     * @param var the name of this variable
     * @param index the variable's index
     * @param value the value of this variable
     */
    public SequenceVariable(String var, Expression index, Expression value) {
        super(var, value);
        
        this.index = index;
    }

    @Override
    public boolean matches(Expression expr) {
        if (super.matches(expr)) {
            return true;
        }
        
        if (expr instanceof SequenceVariable) {
            setValue(expr);
            
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        return String.format("%s[%s]", name, index);
    }
    
    @Override
    public String toUnicode() {
        return String.format("%s%s", name, toSubscript(index));
    }
    
    private static String toSubscript(Expression expr) {
        if (expr instanceof NaturalNumber) {
            String n = expr.toString();
            StringBuilder buf = new StringBuilder();
            int diff = 0x2080 - 0x30;
            
            for (int i = 0; i < n.length(); i++) {
                buf.appendCodePoint(n.codePointAt(i) + diff);
            }
            
            return buf.toString();
        } else {
            return "." + expr;
        }
    }
}
