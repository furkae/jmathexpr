/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.test.arithmetic;

import jmathexpr.Expression;
import jmathexpr.Variable;
import jmathexpr.arithmetic.Numbers;
import jmathexpr.arithmetic.Polynomial;
import jmathexpr.bool.TruthValue;
import jmathexpr.func.Function;
import jmathexpr.arithmetic.func.Exp;
import jmathexpr.arithmetic.func.Log;
import jmathexpr.arithmetic.func.Sqrt;
import jmathexpr.arithmetic.integer.IntegerNumber;
import jmathexpr.arithmetic.integer.Integers;
import jmathexpr.arithmetic.natural.NaturalNumber;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.op.Addition;
import jmathexpr.arithmetic.op.Division;
import jmathexpr.arithmetic.op.Exponentiation;
import jmathexpr.arithmetic.op.Multiplication;
import jmathexpr.arithmetic.op.Subtraction;
import jmathexpr.arithmetic.rational.RationalNumber;
import jmathexpr.arithmetic.rational.Rationals;
import jmathexpr.arithmetic.rational.impl.LongRationalNumber;
import jmathexpr.arithmetic.real.Infinity;
import jmathexpr.arithmetic.real.RealNumber;
import jmathexpr.arithmetic.real.Reals;
import jmathexpr.number.set.Interval;
import jmathexpr.relation.Equality;
import jmathexpr.set.ElementOf;
import jmathexpr.set.OrderedPair;
import jmathexpr.util.context.ExpressionContext;
import jmathexpr.util.parser.ExpressionParser;

import static org.testng.AssertJUnit.assertEquals;
import org.testng.annotations.Test;

/**
 * Base numerical and arithmetic test cases.
 * 
 * @author Elemér Furka
 */
@Test(groups="ArithmeticTest")
public class ArithmeticTest {
    
    private final Naturals N = Naturals.getInstance();
    private final Integers Z = Integers.getInstance();
    private final Rationals Q = Rationals.getInstance();
    private final Reals R = Reals.getInstance();

    public ArithmeticTest() {
    }
    
    @Test(dependsOnMethods = { "testIntervals" })
    public void testPolynomials() {
        Variable x = Numbers.variable("x");
//        Logger.dump(new ExpressionParser().parse("2x^2 - 5x"));
//        System.exit(1);
//        Polynomial a = Polynomial.create(new ExpressionParser().parse("x^3 - 2x^2 - 4"), x);
        Polynomial a = Polynomial.create(new ExpressionParser().parse("x^3 - 2*x^2 - 4"), x);
        Polynomial b = Polynomial.create(new ExpressionParser().parse("x - 3"), x);
        OrderedPair quotient = a.euclideanDivision(b);
        
        System.out.printf("(%s) : (%s) = %s%n", a, b, quotient);
        
        Polynomial q = Polynomial.create(new ExpressionParser().parse("x^2 + x + 3"), x);
        Polynomial r = Polynomial.create(N.create(5), x);
        
        assertEquals(quotient, new OrderedPair(q, r));
    }
    
    @Test(dependsOnMethods = { "testFunctions" })
    public void testIntervals() {
        Expression contains, evaluated;
        Interval interval = new Interval(true, N.create(0), N.create(1), true); // [0, 1]
        System.out.printf("%s%n", interval);

        contains = new ElementOf(R.create(-1), interval);
        evaluated = contains.evaluate();
        System.out.printf("  %s : %s%n", contains, evaluated);
        assertEquals(evaluated, TruthValue.False);
        
        contains = new ElementOf(R.create(0), interval);
        evaluated = contains.evaluate();        
        System.out.printf("  %s : %s%n", contains, evaluated);
        assertEquals(evaluated, TruthValue.True);
        
        interval = new Interval(false, N.create(0), N.create(1), true); // (0, 1]
        System.out.printf("%s%n", interval);
        
        contains = new ElementOf(R.create(0), interval);
        evaluated = contains.evaluate();
        System.out.printf("  %s : %s%n", contains, evaluated);
        assertEquals(evaluated, TruthValue.False);
        
        contains = new ElementOf(Z.create(13), interval);
        evaluated = contains.evaluate();
        System.out.printf("  %s : %s%n", contains, evaluated);
        assertEquals(evaluated, TruthValue.False);
        
        interval = new Interval(false, N.create(0), Infinity.PLUS_INFINITY, false); // (0, inf)
        System.out.printf("%s%n", interval);

        contains = new ElementOf(R.create(3.14), interval);
        evaluated = contains.evaluate();
        System.out.printf("  %s : %s%n", contains, evaluated);
        assertEquals(evaluated, TruthValue.True);
    }
    
    @Test(dependsOnMethods = { "testSum" })
    public void testFunctions() {
        Expression evaluated;
        Variable x = Numbers.variable("x");
        Function exp = new Exp(x);
        System.out.printf("%s%n", exp);
        
        x.setValue(R.create(0));
        evaluated = exp.evaluate();
        System.out.printf("  %s%n", evaluated);
        assertEquals(evaluated, Naturals.one());
        
        x.setValue(R.create(1));
        System.out.printf("  %s%n", exp.evaluate());
        
        Function log = new Log(N.create(10), N.create(1000));
        evaluated = log.evaluate();
        System.out.printf("%s = %s%n", log, evaluated);
        assertEquals(evaluated, N.create(3));
    }
    
    @Test(dependsOnMethods = { "testExponentiation" })
    public void testSum() {
        ExpressionParser parser = new ExpressionParser();
        Expression sum = parser.parse("45/2 + 3/2 * sqrt(249) + 15/8 * sqrt(249) + 237/8 + 1");
        Expression evaluated = sum.evaluate();
        System.out.printf("%s = %s%n", sum, evaluated);
        
        Expression expected = parser.parse("(27sqrt(249) + 425) / 8");
        assertEquals(evaluated, expected);
        
        Variable x = Numbers.variable("x");
        x.setValue(parser.parse("(15 + sqrt(249)) / 4"));
        ExpressionContext.getInstance().addVariable(x);
        Expression quadratic = parser.parse("x^2 - 6x + 1");
        evaluated = quadratic.evaluate();
        System.out.printf("%s (%s = %s) = %s%n", quadratic, x.name(), x.evaluate(), evaluated);
        
        expected = parser.parse("(3 sqrt(249) + 65) / 8");
        assertEquals(evaluated, expected);
    }
    
    @Test(dependsOnMethods = { "testRealExpressions" })
    public void testExponentiation() {
        Exponentiation exp3 = new Exponentiation(N.create(5), N.create(3));
        Expression evaluated = exp3.evaluate();
        System.out.printf("%s = %s%n", exp3, evaluated);
        assertEquals(evaluated, N.create(125));
    }

    @Test(dependsOnMethods = { "testRationalExpressions" })
    public void testRealExpressions() {
        Expression evaluated;
        RealNumber r0d1 = R.create(0.1);
        RealNumber div = R.create(1.0/10);
        Subtraction diff = new Subtraction(r0d1, div);
        
        RealNumber zero = R.create(0.0);
        Equality zeroTest = new Equality(diff, zero);
        
        evaluated = zeroTest.evaluate();
        System.out.printf("%s : %s%n", zeroTest, evaluated);
        assertEquals(evaluated, TruthValue.True);
        
        Division division = new Division(R.create(10.0), R.create(2.0));
        System.out.printf("Division domain: %s%n", division.domain());
        
        ExpressionParser parser = new ExpressionParser();
        Expression fraction = parser.parse("(6 sqrt(249) + 130) / 16");
        evaluated = fraction.evaluate();
        System.out.printf("%s : %s%n", fraction, evaluated);
        
        Expression expected = parser.parse("(3 sqrt(249) + 65) / 8");
        assertEquals(evaluated, expected);
    }
    
    @Test(dependsOnMethods = { "testVariables" })
    public void testRationalExpressions() {
        Expression evaluated;
        RationalNumber oneHalf = new LongRationalNumber(1, 2);
        RationalNumber oneThird = new LongRationalNumber(1, 3);
        Expression sum = new Addition(oneHalf, oneThird);
        
        evaluated = sum.evaluate();
        System.out.printf("%s = %s%n", sum, evaluated);
        assertEquals(evaluated, new LongRationalNumber(5, 6));
        
        Expression difference = new Subtraction(oneHalf, oneThird);
        evaluated = difference.evaluate();
        System.out.printf("%s = %s%n", difference, evaluated);
        assertEquals(evaluated, new LongRationalNumber(1, 6));
        
        RationalNumber sixEighth = new LongRationalNumber(6, 8);
        evaluated = sixEighth.evaluate();
        System.out.printf("%s = %s%n", sixEighth, evaluated);
        assertEquals(evaluated, new LongRationalNumber(3, 4));
        
        difference = new Subtraction(new LongRationalNumber(2, 3), new LongRationalNumber(1, 6));
        evaluated = difference.evaluate();
        System.out.printf("%s = %s%n", difference, evaluated);
        assertEquals(evaluated, new LongRationalNumber(1, 2));
        
        Expression product = new Multiplication(new LongRationalNumber(2, 3), new LongRationalNumber(9, 2));
        ElementOf isNatural = new ElementOf(product, N);
        evaluated = isNatural.evaluate();
        System.out.printf("%s = %s : %s%n", product.evaluate(), isNatural, evaluated);
        assertEquals(evaluated, TruthValue.True);
        
        System.out.println(Q.create("-3.750"));
    }
    
    @Test(dependsOnMethods = { "testIntegerExpressions" })
    public void testVariables() {
        NaturalNumber two = N.create(2);
        Variable x = Numbers.variable("x"), a = new Variable("a", new Multiplication(two, x));
              
        for (int i = 1; i < 4; i++) {
            x.setValue(N.create(i));
            System.out.printf("  %s = %s%n", a, a.evaluate());
        }
        
        assertEquals(a.evaluate(), N.create(6));
    }
    
    @Test(dependsOnMethods = { "testNaturalExpressions" })
    public void testIntegerExpressions() {
        IntegerNumber ten = Z.create(10);
        IntegerNumber eleven = Z.create(11);  
        Expression integerExpr = new Subtraction(ten, eleven);
        
        System.out.printf("%s = %s%n", integerExpr, integerExpr.evaluate());        
        assertEquals(integerExpr.evaluate(), Z.create(-1));
    }
    
    @Test
    public void testNaturalExpressions() {
        Expression evaluated;
        NaturalNumber five = N.create(5);
        NaturalNumber two = N.create(2);
        Addition seven = new Addition(five, two);
        NaturalNumber six = N.create("6");
        Expression naturalExpr = new Multiplication(six, seven);
        
        evaluated = naturalExpr.evaluate();
        System.out.printf("%s = %s%n", naturalExpr, evaluated);        
        assertEquals(evaluated, N.create(42));
        
        Expression equality = new Equality(naturalExpr, N.create(42));
        evaluated = equality.evaluate();
        System.out.printf("%s : %s%n", equality, evaluated);
        assertEquals(evaluated, TruthValue.True);
                
        Expression elementOf = new ElementOf(naturalExpr, N);
        evaluated = elementOf.evaluate();
        System.out.printf("%s : %s%n", elementOf, evaluated);
        assertEquals(evaluated, TruthValue.True);
        
        elementOf = new ElementOf(six, N);
        evaluated = elementOf.evaluate();
        System.out.printf("%s : %s%n", elementOf, evaluated);
        assertEquals(evaluated, TruthValue.True);
        
        Expression sqrt = new Sqrt(N.create(45));
        evaluated = sqrt.evaluate();
        System.out.printf("%s = %s%n", sqrt, evaluated);
        assertEquals(evaluated, new Multiplication(N.create(3), new Sqrt(N.create(5))));
    }
}
