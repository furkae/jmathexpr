/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.test.arithmetic.equation;

import jmathexpr.Expression;
import jmathexpr.Variable;
import jmathexpr.arithmetic.equation.Equation;
import jmathexpr.arithmetic.equation.EquationSolveException;
import jmathexpr.arithmetic.integer.Integers;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.rational.Rationals;
import jmathexpr.set.FiniteSet;
import jmathexpr.set.Set;
import jmathexpr.util.parser.ExpressionParser;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.Test;

/**
 * TestNG absolute value equation test cases.
 * 
 * @author Elemér Furka
 */
@Test(groups="AbsoluteValueEquationTest")
//@Test(groups="AbsoluteValueEquationTest", dependsOnGroups="RadicalEquationTest")
public class AbsoluteValueEquationTest {
    
    public AbsoluteValueEquationTest() {
    }

    @Test
    public void linear() throws EquationSolveException {
        Equation abs = (Equation) new ExpressionParser().parse("|2x - 1| = 5");
        Variable x = abs.variable();
        System.out.printf("%s : %s = ?%n", abs, x);
        
        Set roots = abs.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression expected = new FiniteSet(Naturals.getInstance().create(3), Integers.getInstance().create(-2));
        assertEquals(expected, roots);
    }

    @Test(dependsOnMethods = { "linear" })
    public void toIsolate() throws EquationSolveException {
        Equation abs = (Equation) new ExpressionParser().parse("|5x - 6| + 3 = 10");
        Variable x = abs.variable();
        System.out.printf("%s : %s = ?%n", abs, x);
        
        Set roots = abs.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression expected = new FiniteSet(Rationals.getInstance().create(13, 5), Rationals.getInstance().create(-1, 5));
        assertEquals(expected, roots);
    }

    @Test(dependsOnMethods = { "toIsolate" })
    public void twoAbsExpressions() throws EquationSolveException {
        Equation abs = (Equation) new ExpressionParser().parse("|2x - 1| = |4x + 3|");
        Variable x = abs.variable();
        System.out.printf("%s : %s = ?%n", abs, x);
        
        Set roots = abs.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression expected = new FiniteSet(Integers.getInstance().create(-2), Rationals.getInstance().create(-1, 3));
        assertEquals(expected, roots);
    }

    @Test(dependsOnMethods = { "twoAbsExpressions" })
    public void quadratic() throws EquationSolveException {
        ExpressionParser parser = new ExpressionParser();
        Equation abs = (Equation) parser.parse("|x^2 - 6x + 1| = |(3x + 5)/2|");
        Variable x = abs.variable();
        System.out.printf("%s : %s = ?%n", abs, x);
        
        Set roots = abs.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression x1 = parser.parse("(15 + sqrt(249)) / 4");
        Expression x2 = parser.parse("(15 - sqrt(249)) / 4");
        Expression expected = new FiniteSet(x1, x2, Rationals.getInstance().create(7, 2), Naturals.one());
        assertEquals(expected, roots);
    }

    @Test(dependsOnMethods = { "quadratic" })
    public void anotherQuadratic() throws EquationSolveException {
        ExpressionParser parser = new ExpressionParser();
        Equation abs = (Equation) parser.parse("|x| = x^2 + x - 3");
        Variable x = abs.variable();
        System.out.printf("%s : %s = ?%n", abs, x);
        
        Set roots = abs.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression x1 = parser.parse("sqrt(3)");
        Expression x2 = Integers.getInstance().create("-3");
        Expression expected = new FiniteSet(x1, x2);
        assertEquals(expected, roots);
    }
}
