/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.test.arithmetic.equation;

import jmathexpr.Expression;
import jmathexpr.Variable;
import jmathexpr.arithmetic.equation.Equation;
import jmathexpr.arithmetic.equation.EquationSolveException;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.rational.Rationals;
import jmathexpr.set.EmptySet;
import jmathexpr.set.FiniteSet;
import jmathexpr.set.Set;
import jmathexpr.util.logging.Logger;
import jmathexpr.util.parser.ExpressionParser;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.Test;

/**
 * TestNG absolute value equation test cases.
 * 
 * @author Elemér Furka
 */
@Test(groups="QuadraticEquationTest")
//@Test(groups="QuadraticEquationTest", dependsOnGroups="AbsoluteValueEquationTest")
public class QuadraticEquationTest {
    
    public QuadraticEquationTest() {
    }

    @Test
    public void quadraticFormula() throws EquationSolveException {
        ExpressionParser parser = new ExpressionParser();
//        Expression test = parser.parse("x^2 - 5x");
//        Logger.dump(test);
//        System.exit(-1);
        Equation quad = (Equation) parser.parse("x^2 - 5x + 3 = 0");
        Variable x = quad.variable();
        System.out.printf("%s : %s = ?%n", quad, x);

        Set roots = quad.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression x1 = parser.parse("(5 + sqrt(13)) / 2");
        Expression x2 = parser.parse("(5 - sqrt(13)) / 2");
        Expression expected = new FiniteSet(x1, x2);
        assertEquals(expected, roots);
    }

    @Test(dependsOnMethods = { "quadraticFormula" })
    public void anotherQuadratic() throws EquationSolveException {
        ExpressionParser parser = new ExpressionParser();
        Equation quad = (Equation) parser.parse("-x^2 + 6x - 8 = 3x + 7");
        Variable x = quad.variable();
        System.out.printf("%s : %s = ?%n", quad, x);
        
        Set roots = quad.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression expected = new EmptySet();
        assertEquals(expected, roots);
    }

    @Test(dependsOnMethods = { "quadraticFormula" })
    public void third() throws EquationSolveException {
        ExpressionParser parser = new ExpressionParser();
//        Equation quad = (Equation) parser.parse("2x^2 - x - 1 = 0");
        Equation quad = (Equation) parser.parse("2*x^2 - x - 1 = 0");
        Variable x = quad.variable();
        System.out.printf("%s : %s = ?%n", quad, x);

        Set roots = quad.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression expected = new FiniteSet(Naturals.one(), Rationals.getInstance().create(-1, 2));
        assertEquals(expected, roots);
    }

    @Test(dependsOnMethods = { "third" })
    public void fraction() throws EquationSolveException {
        ExpressionParser parser = new ExpressionParser();
//        Equation quad = (Equation) parser.parse("1/2 x^2 - 16x = 5");
        Equation quad = (Equation) parser.parse("1/2*x^2 - 16x = 5");
        Variable x = quad.variable();
        System.out.printf("%s : %s = ?%n", quad, x);

        Set roots = quad.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression x1 = parser.parse("16 + sqrt(266)");
        Expression x2 = parser.parse("16 - sqrt(266)");
        Expression expected = new FiniteSet(x1, x2);
        assertEquals(expected, roots);
    }

    @Test(dependsOnMethods = { "fraction" })
    public void irrational() throws EquationSolveException {
        ExpressionParser parser = new ExpressionParser();
//        Equation quad = (Equation) parser.parse("sqrt(3) x^2 + sqrt(5) x = 12");
        Equation quad = (Equation) parser.parse("sqrt(3)*x^2 + sqrt(5) x = 12");
        Variable x = quad.variable();
        System.out.printf("%s : %s = ?%n", quad, x);

        Set roots = quad.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        Expression x1 = parser.parse("(sqrt(15 + 144sqrt(3)) - sqrt(15)) / 6");
        Expression x2 = parser.parse("(-sqrt(15) - sqrt(15 + 144sqrt(3))) / 6");
        Expression expected = new FiniteSet(x1, x2);
        assertEquals(expected, roots);
    }
}
