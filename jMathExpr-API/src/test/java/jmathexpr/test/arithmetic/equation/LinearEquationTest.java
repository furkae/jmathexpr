/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.test.arithmetic.equation;

import jmathexpr.Expression;
import jmathexpr.Variable;
import jmathexpr.arithmetic.equation.Equation;
import jmathexpr.arithmetic.equation.EquationSolveException;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.arithmetic.rational.Rationals;
import jmathexpr.relation.Equality;
import jmathexpr.set.FiniteSet;
import jmathexpr.set.Set;
import jmathexpr.util.parser.ExpressionParser;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.Test;

/**
 * TestNG linear equation test cases.
 * 
 * @author Elemér Furka
 */
@Test(groups="LinearEquationTest", enabled=false)
public class LinearEquationTest {
    
    public LinearEquationTest() {
    }

    @Test
    public void simple() throws EquationSolveException {
        Equation lineq = (Equation) new ExpressionParser().parse("4*x + 3 = x + 27");
        Variable x = lineq.variable();
        System.out.printf("%s : %s = ?%n", lineq, x);
        
        Set    roots = lineq.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        assertEquals(roots, new FiniteSet(Naturals.getInstance().create(8)));
    }

    @Test(dependsOnMethods = { "simple" })
    public void parenthesis() throws EquationSolveException {
        Equation lineq = (Equation) new ExpressionParser().parse("2*(3*x - 7) + 4*(3*x + 2) = 6*(5*x + 9 ) + 3");
        Variable x = lineq.variable();
        System.out.printf("%s : %s = ?%n", lineq, x);
        
        Set    roots = lineq.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        assertEquals(roots, new FiniteSet(Rationals.getInstance().create(-21, 4)));
    }

    @Test(dependsOnMethods = { "parenthesis" })
    public void rationals() throws EquationSolveException {
//        Equation lineq = (Equation) new ExpressionParser().parse("3/4 x + 5/6 = 5x - 125/3");
        Equation lineq = (Equation) new ExpressionParser().parse("3/4*x + 5/6 = 5x - 125/3");
        Variable x = lineq.variable();
        System.out.printf("%s : %s = ?%n", lineq, x);
        
        Set    roots = lineq.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        assertEquals(roots, new FiniteSet(Naturals.getInstance().create(10)));
    }

    @Test(dependsOnMethods = { "rationals" })
    public void fractions() throws EquationSolveException {
        Equation lineq = (Equation) new ExpressionParser().parse("(6x - 7)/4 + (3x - 5)/7 = (5x + 78)/28");
        Variable x = lineq.variable();
        System.out.printf("%s : %s = ?%n", lineq, x);
        
        Set    roots = lineq.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        assertEquals(roots, new FiniteSet(Naturals.getInstance().create(3)));
    }

    @Test(dependsOnMethods = { "fractions" })
    public void irrationalConstants() throws EquationSolveException {
        Equation lineq = (Equation) new ExpressionParser().parse("sqrt(2) x - sqrt(3) = sqrt(5)");
        Variable x = lineq.variable();
        System.out.printf("%s : %s = ?%n", lineq, x);
        
        Set roots = lineq.solve();
        System.out.printf("  %s = %s%n", x, roots);

//        for (Equality e : lineq.getSteps()) {
//            System.out.printf("    %s%n", e);
//        }
        
        Expression expected = new ExpressionParser().parse("(sqrt(5) + sqrt(3)) / sqrt(2)");
        
        assertEquals(roots, new FiniteSet(expected));
    }
}
