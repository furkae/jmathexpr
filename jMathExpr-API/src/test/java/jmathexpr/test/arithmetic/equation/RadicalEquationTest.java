/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.test.arithmetic.equation;

import jmathexpr.Variable;
import jmathexpr.arithmetic.equation.Equation;
import jmathexpr.arithmetic.equation.EquationSolveException;
import jmathexpr.arithmetic.natural.Naturals;
import jmathexpr.relation.Equality;
import jmathexpr.set.EmptySet;
import jmathexpr.set.FiniteSet;
import jmathexpr.set.Set;
import jmathexpr.util.parser.ExpressionParser;

import static org.testng.AssertJUnit.assertEquals;

import org.testng.annotations.Test;

/**
 * TestNG radical equation test cases.
 * 
 * @author Elemér Furka
 */
//@Test(groups="RadicalEquationTest", dependsOnGroups="LinearEquationTest", enabled=false)
@Test(groups="RadicalEquationTest")
public class RadicalEquationTest {
    
    public RadicalEquationTest() {
    }

    @Test
    public void oneRadical() throws EquationSolveException {
        Equation req = (Equation) new ExpressionParser().parse("sqrt(x - 8) = 3");
        Variable x = req.variable();
        System.out.printf("%s : %s = ?%n", req, x);
        
        Set roots = req.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        assertEquals(roots, new FiniteSet(Naturals.getInstance().create(17)));
    }

    @Test(dependsOnMethods = { "oneRadical" })
    public void toIsolate() throws EquationSolveException {
        Equation req = (Equation) new ExpressionParser().parse("sqrt(x - 10) - 4 = 0");
        Variable x = req.variable();
        System.out.printf("%s : %s = ?%n", req, x);
        
        Set roots = req.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        assertEquals(roots, new FiniteSet(Naturals.getInstance().create(26)));
    }

    @Test(dependsOnMethods = { "toIsolate" })
    public void radicalAndLinear() throws EquationSolveException {
        Equation req = (Equation) new ExpressionParser().parse("sqrt(x + 1) - 3x = 1");
        Variable x = req.variable();
        System.out.printf("%s : %s = ?%n", req, x);
        
        Set roots = req.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        assertEquals(roots, new FiniteSet(Naturals.getInstance().create(0)));
    }

    @Test(dependsOnMethods = { "radicalAndLinear" })
    public void twoRadicals() throws EquationSolveException {
        Equation req = (Equation) new ExpressionParser().parse("sqrt(x) + sqrt(x - 5) = 1");
        Variable x = req.variable();
        System.out.printf("%s : %s = ?%n", req, x);
        
        Set roots = req.solve();
        System.out.printf("  %s = %s%n", x, roots);
        
        assertEquals(roots, new EmptySet());
    }

    @Test(dependsOnMethods = { "twoRadicals" })
    public void threeRadicals() throws EquationSolveException {
        Equation req = (Equation) new ExpressionParser().parse("sqrt(x + 8) + sqrt(x + 15) = sqrt(9x + 40)");
        Variable x = req.variable();
        System.out.printf("%s : %s = ?%n", req, x);
        
        Set roots = req.solve();
        System.out.printf("  %s = %s%n", x, roots);

//        for (Equality s : req.getSteps()) {
//            System.out.printf("    %s%n", s);
//        }
        
        assertEquals(roots, new FiniteSet(Naturals.one()));
    }
}
