/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.test;

import jmathexpr.Expression;
import jmathexpr.util.logging.Logger;
import jmathexpr.util.parser.ExpressionParser;

import org.testng.annotations.Test;

/**
 * Test cases for the expression parser. They should normally not be part of
 * the regression test suites.
 * 
 * @author Elemér Furka
 */
@Test(groups="ParserTest")
public class ParserTest {
    
    public ParserTest() {
    }
    
    @Test
    public void testPolynomials() {
        ExpressionParser parser = new ExpressionParser();
        
        Expression expression = parser.parse("let x in R");
        
        Logger.dump(expression);
    }
}
