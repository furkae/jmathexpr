/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.web.controller;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import jmathexpr.util.context.ExpressionContext.Item;
import jmathexpr.util.context.Statement;

import jmathexpr.web.ejb.ExpressionServiceLocal;

/**
 * Backing bean for user inputs.
 * 
 * @author Elemér Furka
 */
@SessionScoped
@ManagedBean(name = "whiteboard")
public class ExpressionsBean implements Serializable {
    
    private String actual;
    
    @EJB
    private ExpressionServiceLocal service;

    public String getActual() {
        return actual;
    }

    public void setActual(String expression) {
        actual = expression;
        
        Statement statement = service.add(expression);
        
        service.execute(statement);
    }    

    public List<Item> getEntries() {
        return service.getContext().getFirstLevelItems();
    }
}
