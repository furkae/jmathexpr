/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.web.ejb;

import javax.ejb.Stateful;

import jmathexpr.arithmetic.equation.Equation;
import jmathexpr.arithmetic.equation.EquationSolveException;
import jmathexpr.set.Set;
import jmathexpr.util.context.ExpressionContext;
import jmathexpr.util.context.Statement;
import jmathexpr.util.context.Statement.Command;
import jmathexpr.util.parser.ExpressionParser;

/**
 * This class represents a whiteboard where expressions are written.
 * 
 * @author Elemér Furka
 */
@Stateful
public class ExpressionService implements ExpressionServiceLocal {
    
    private final ExpressionParser parser = new ExpressionParser();
    
    @Override
    public Statement add(String stmt) {
        Statement statement = parser.parseStatement(stmt);
        
        ExpressionContext.getInstance().addStatement(statement);
        
        return statement;
    }

    @Override
    public ExpressionContext getContext() {
        return ExpressionContext.getInstance();
    }

    @Override
    public void execute(Statement statement) {
        switch (statement.getCommand()) {
            case Solve:
                Equation eq = (Equation) statement.getExpression();
                
                try {
                    Set result = eq.solve();
                    Statement expression = new Statement(Command.Expression, result);
                    
                    ExpressionContext.getInstance().addStatement(expression);
                } catch (EquationSolveException e) {
                    System.err.println(e);
                }
            
                break;
                
            case Expression:
            case Let:
                break; // nothing to do
                
            default:
                throw new IllegalStateException("Unexpected statement: " + statement);
        }
    }
}
