/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jmathexpr.web.ejb;

import javax.ejb.Local;
import jmathexpr.util.context.ExpressionContext;

import jmathexpr.util.context.Statement;


/**
 * The set of methods accessible for the web client.
 * 
 * @author Elemér Furka
 */
@Local
public interface ExpressionServiceLocal {

    /**
     * Adds a new statement to the whiteboard entries.
     * 
     * @param statement input statement as a string
     * @return the parsed statement
     */
    Statement add(String statement);
    
    /**
     * Returns the actual expression context.
     * 
     * @return the actual expression context
     */
    ExpressionContext getContext();
    
    /**
     * Executes the given statement. The result (along with the performed steps)
     * are put into this thread's expression context.
     * 
     * @param statement the statement to be executed
     */
    void execute(Statement statement);
}
